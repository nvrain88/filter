@extends('computer.home.master')
@section('title','Trang chủ')
@section('content')

    <div class="wrapper_main container">

        <!-- breadcrumb  -->
        <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
          <li><i class="fa fa-chevron-right"></i><span></span></li>
        </ul>
      </div> 
        <!-- breadcrumb  -->



        <!-- detail category -->
        <div class="row">

        <!-- sidebar -->
        @include('computer.home.sidebar_right')
        <!-- sidebar -->

            <!-- content -->
            <div class="col-md-9">

                <!-- block_des_category -->
                <div class="block_des_category">
                    <h4>Kết Quả Tìm Kiếm</h4>
                </div>
                <!-- block_des_category -->

            <!--
          <div class="block_sort_category">
            <div align="right">
              <label>{{ trans('category.orderby') }} </label>
              @if($path_type == "category")
                <select id="select_productstt" base_url="{{ url('') }}" path_type="{{ $path_type }}" path_id="{{ $path_id }}" >
                  <option value="">---</option>
                  @foreach($productstt as $item)
                    <option value="{{ $item->id }}">{{ $item->sttname }}</option>
                  @endforeach
                        </select>
@elseif($path_type == "filter")
                <select id="select_productstt" base_url="{{ url('') }}" path_type="{{ $path_type }}" color="{{ $color }}" range="{{ $range }}" >
                  <option value="">---</option>
                  @foreach($productstt as $item)
                    <option value="{{ $item->id }}">{{ $item->sttname }}</option>
                  @endforeach
                        </select>
@elseif($path_type == "search")
                <select id="select_productstt" base_url="{{ url('') }}" path_type="{{ $path_type }}" key="{{ $key }}" >
                  <option value="">---</option>
                  @foreach($productstt as $item)
                    <option value="{{ $item->id }}">{{ $item->sttname }}</option>
                  @endforeach
                        </select>
@endif
                    </div>
                  </div>
-->
                <?php
                    @$stt = $_GET['stt'];
                    @$range = $_GET['range'];
                ?>

                <!-- block_product_category -->
                <div class="block_product_category">
                    @if($list_product_cat->count()>0)
                        @foreach($list_product_cat as $item)
                            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" style="padding-right:3px; padding-left: 3px;">
                                 <div class="product-item">
                                      <div class="pi-img-wrapper">
                                        <img src="{{ asset('public/img/product/'.$item->image) }}" alt="Berry Lace Dress" width="300px" height="200px">
                                        <div>
                                          <a href="{{ url('san-pham/'.$item->slug) }}" class="btn">Xem</a>
                                          <a href="{{ url('san-pham/'.$item->slug) }}" class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                        </div>
                                      </div>
                                      <p class="product-name"><a href="{{ url('san-pham/'.$item->slug) }}">{{ $item->name }}</a></p>
                                       <div class="product-price">
                                        <span>{{ format_curency($item->price) }}</span>
                                        @if($item->price_compare != 0)
                                            <span class="old_price">{{ format_curency($item->price_compare) }} </span>
                                        @endif
                                    </div>
                                        @if($item->quantity != 0)
                                            <button class="btn btn_add_cart_main" idproduct="{{ $item->idproduct }}" base_url="{{ route('home.showProduct') }}" token="{{ csrf_token() }}"><i class="fa fa-cart-plus fa-1x"><span> MUA NGAY</span></i></button>
                                        @else
                                            <a class="btn btn_add_cart_main" href="#"><i class="fa fa-comments-o fa-1x"><span> LIÊN HỆ</span></i></a>
                                        @endif
                                      <div class="sticker sticker-new" style="background: url( {{ url('public/img/listproduct/'.$item->tinhtrang['sttimg'])    }} ) no-repeat;">
                                          
                                      </div>
                                    </div>
                            </div>
                        @endforeach
                    @else
                        <h5>Không có sản phẩm nào phù hợp với từ khóa tìm kiếm !</h5 >
                    @endif
                </div>
                <!-- block_product_category -->


                <!-- pagination -->
                <div class="page_bottom">
                {!! $list_product_cat->render() !!}
                <!-- <p class="info">{{ trans('category.pagination_show') }} 20 {{ trans('category.pagination_of') }} 300 {{ trans('category.pagination_product') }}</p> -->
                </div>
                <!-- pagination -->
            </div>
            <!-- content -->

        </div>
        <!-- detail category -->

        <style type="text/css">
            .filter_main{
                /*display: none;*/
            }
        </style>

    </div>

@endsection()