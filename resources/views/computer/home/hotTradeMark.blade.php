@extends('computer.home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('css')
    <style>
        .logo-brand {
            position: relative;
            display: block;
            float: left;
            width: 70px;
            height: 70px;
            line-height: 70px;
            padding: 3px;
            margin-left: 0;
            background: #fff;
            color: #fff;
            font-size: 14px;
            text-align: center;
            border: 1px solid #eaeaea;
        }
        .view_pc .logo-brand img {
            position: relative;
            display: block;
            width: initial;
            max-width: 100%;
            max-height: 100%;
            margin: auto;
            object-fit: scale-down;
        }
        .view_pc .name-brand {
            position: relative;
            display: block;
            width: 100%;
            height: 35px;
            line-height: 38px;
            padding: 0;
            margin-left: 10px;
            background: transparent;
            color: #333;
            font-size: 20px;
            text-align: left;
            font-weight: 700;
            white-space: nowrap;
            overflow: hidden;
            text-overflow: ellipsis;
        }
        .view_pc .btn-go-store {
            position: relative;
            display: block;
            width: 100%;
            height: 20px;
            line-height: 20px;
            padding: 0;
            margin-left: 10px;
            color: #00537a;
            font-size: 14px;
            text-align: left;
            text-decoration: underline;
        }
        .view_pc .box-name {
            position: relative;
            display: block;
            float: left;
        }
        .block-trade-mark {
            margin-bottom: 10px;
            margin-top: 20px;
            padding-bottom: 10px;
            padding-top: 20px;
            background-color: #f5f9fc;
        }
        .view_pc .box-brand {
            position: relative;
            display: block;
            background: #fff;
            clear: both;
            overflow: hidden;
        }
        .view_pc .box-brand .list-logo-brand {
            overflow: hidden;
        }
        .item-brand {
            position: relative;
            display: block;
            overflow: hidden;
            width: calc(100% / 6);
            float: left;
            margin: 0;
            padding: 12.5px 15px 12.5px 0px;
        }
        
        .item-brand:nth-child(6){
            padding-right: 0px;
        }
        
        .view_pc .box-brand .list-logo-brand .item-brand .box-inner {
            background: #f5f9fc;
            overflow: hidden;
            padding: 10px;
            border-radius: 6px;
        }
        .view_pc .box {
            position: relative;
            display: block;
        }
        .box-img {
            position: relative;
            display: block;
            float: left;
            width: 100%;
            height: 100px;
            max-height: 122px;
        }
        .box-img img {
            margin: auto;
            object-fit: scale-down;
        }
        .view_pc img {
            vertical-align: middle;
            border: 0;
        }
        img.responsive {
            position: relative;
            display: block;
            width: 100%;
            height: 100%;
            object-fit: contain;
        }
        
        .view_pc .box-product{
            padding-left: 0px;
            padding-right: 0px;
            margin-top: 10px;
        }
        
        .view_pc .box-product .col-lg-3{
            padding-left: 0px;
            padding-right: 15px;
        }
        
        .view_pc .box-product .col-lg-3:nth-child(4){
            padding-right: 0px;
        }
        
    </style>
@endsection
@section('content')

    <div class="wrapper_main container view_pc">
        <!-- quang cáo -->

        <!-- breadcrumb  -->
        <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
          <li><i class="fa fa-chevron-right"></i><span>Thương hiệu lớn</span></li>
        </ul>
      </div> 
        <!-- breadcrumb  -->


        <!-- detail category -->
        <div class="row">
            <!-- content -->
            <div class="col-md-12 search-trend">
                <section class="box-brand clearfix">
                    <div class="box">
                        <div class="box list-logo-brand">
                            @foreach($tradeMarks as $tradeMark)
                                @if ($tradeMark->status == 1)
                                <div class="box item-brand">
                                    <div class="box box-inner">
                                        <a class="box-img" href="{{ route('home.trademark',$tradeMark->id) }}">
                                            <img src="{{asset('public/img/trademark/'.$tradeMark->image)}}" class="responsive">
                                        </a>
                                    </div>
                                </div>
                                @endif
                            @endforeach
                        </div>
                    </div>
                </section>
                <div class="box">
                    @foreach($tradeMarks as $tradeMark)
                        @if ($tradeMark->status == 1)
                            <div class="col-md-12 block-trade-mark">
                            <div class="wrap-title-brand">
                                <a href="{{ route('home.trademark',$tradeMark->id) }}" title="{{$tradeMark->title}}" class="logo-brand">
                                    <img src="{{asset('public/img/trademark/'.$tradeMark->image)}}" alt="OPPO" class="responsive" >
                                </a>
                                <div class="box-name">
                                    <a href="{{ route('home.trademark',$tradeMark->id) }}" title="{{$tradeMark->title}}" class="name-brand">{{$tradeMark->title}}</a>
                                    <a href="{{ route('home.trademark',$tradeMark->id) }}" title="{{$tradeMark->title}}" class="btn-go-store">Xem chi tiết</a>
                                </div>
                            </div>
                        <div class="box-product col-md-12">
                            @foreach($tradeMark->hotProducts as $itemproduct)
                                <div class="col-xs-12 col-sm-6 col-md-3 col-lg-3">
                                    <div class="product-item">
                                        <div class="pi-img-wrapper">
                                            <img src="{{ asset('public/img/product/'.$itemproduct->image) }}"
                                                 alt="Berry Lace Dress" width="300px" height="200px">
                                            <div>
                                                <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                                                <a href="{{ url('san-pham/'.$itemproduct->slug) }}"
                                                   class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                            </div>
                                        </div>
                                        <p class="product-name"><a
                                                    href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a>
                                        </p>
                                        <div class="product-price">
                                            <span>{{ format_curency($itemproduct->price) }}</span>
                                            @if($itemproduct->price_compare != 0)
                                                <span class="old_price">{{ format_curency($itemproduct->price_compare) }} </span>
                                            @endif
                                        </div>
                                        @if($itemproduct->quantity != 0)
                                            <button class="btn btn_add_cart_main"
                                                    idproduct="{{ $itemproduct->idproduct }}"
                                                    base_url="{{ route('home.showProduct') }}"
                                                    token="{{ csrf_token() }}"><i
                                                        class="fa fa-cart-plus fa-1x"><span> MUA NGAY</span></i>
                                            </button>
                                        @else
                                            <a class="btn btn_add_cart_main" href="#"><i
                                                        class="fa fa-comments-o fa-1x"><span> LIÊN HỆ</span></i></a>
                                        @endif
                                        <div class="sticker sticker-new"
                                             style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">

                                        </div>
                                    </div>
                                </div>
                            @endforeach

                        </div>
                        </div>
                        @endif
                    @endforeach
                </div>

            </div>
            <!-- content -->

            <!-- sidebar -->
            <!-- sidebar -->

        </div>
        <!-- detail category -->

    </div>
@endsection()
@section('css')

@stop