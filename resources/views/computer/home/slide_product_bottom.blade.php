<!-- product sale -->
<div class="block_product_related category-post-section"  id="category-post-section"> 

    <!-- Section Header -->
        <h2 style="text-align:center;">{{ trans('product.related') }}</h2>

    <!-- Section Header /- -->
  
    <!-- Wrapper for slides -->
    <div id="responsive-sale">
        @foreach($listproducts_bottom as $itemproduct)
            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3" style="padding-right:3px; padding-left: 3px;">
                <div class="product-item">
                                  <div class="pi-img-wrapper">
                                    <img src="{{ asset('public/img/product/'.$itemproduct->image) }}" alt="Berry Lace Dress" width="300px" height="200px">
                                    <div>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                    </div>
                                  </div>
                                  <p class="product-name"><a href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a></p>
                                  <div class="product-price">
                                        @if($itemproduct->price != 0)
                                        <span>{{ format_curency($itemproduct->price) }}</span>
                                        @if($itemproduct->price_compare != 0)
                                                <span class="old_price">{{ format_curency($itemproduct->price_compare) }} </span>
                                        @endif
                                        @else
                                        <span style="text-transform: uppercase">{{ trans("index.contact") }}</span>
                                        @endif
                                    </div>
                                        @if($itemproduct->price != 0 && !empty($itemproduct->price))
                                            <button class="btn btn_add_cart_main" idproduct="{{ $itemproduct->idproduct }}" base_url="{{ route('home.showProduct') }}" token="{{ csrf_token() }}"><i class="fa fa-cart-plus fa-1x"><span class="hidden-xs"> MUA NGAY</span></i></button>
                                        @endif
                                  <div class="sticker sticker-new" style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">
                                      
                                  </div>
                                </div>
            </div> 
        @endforeach
    </div>
    <!-- Wrapper for slides -->
</div> 
<!-- end product sale -->