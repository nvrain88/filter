@extends('computer.home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('css')
    <style>
        #support_page .row {
            display: -ms-flexbox;
            display: flex;
            -ms-flex-wrap: wrap;
            flex-wrap: wrap;
            padding: 0 4px;
        }
        #support_page .title_support_page {
            font-size: 19px;
            font-weight: 700;
            padding-bottom: 10px;
            margin-bottom: 10px;
            border-bottom: 1px solid #e5e5e5;
        }
        .tab-content>.active {
            display: block;
        }
        .item_support_page {
            padding: 10px;
            border: 1px solid #eaeaea;
            margin-bottom: 10px;
            width: 100%;
            float: left;
        }
        a:visited, .alink:visited, a.txt_link_hasaki, a, a:focus, a:active, .alink {
            color: #333333;
            text-decoration: none;
            outline: none;
        }
        .title_fck {
            font-weight: 700;
            margin-bottom: 10px;
        }
        .box_support_page {
            border: 1px solid #e5e5e5;
        }

        .space_bottom_10 {
            margin-bottom: 10px;
        }
        .box_support_page .nav-tabs {
            border: none;
        }
        .box_support_page .block_item_content_support {
            border-bottom: 1px solid #e5e5e5;
            padding: 0 10px 10px 10px;
        }
        .box_support_page .title_support {
            margin: 0 0px 10px 0px;
            border-bottom: 1px dotted #e5e5e5;
            padding: 10px 0;
            font-size: 17px;
        }
        .box_support_page .nav-tabs> li {
            width: 100%;
            border: none;
            position: relative;
        }
        .item_support_page {
            padding: 10px;
            border: 1px solid #eaeaea;
            margin-bottom: 10px;
            width: 100%;
            float: left;
        }
        .nav-tabs>li {
            float: left;
            margin-bottom: -1px;
        }
        .nav>li {
            position: relative;
            display: block;
        }
        .box_support_page .nav-tabs> li.active> a, .box_support_page .nav-tabs> li.active> a:hover, .box_support_page .nav-tabs> li.active> a:focus {
            font-weight: 700;
            color: #326e51;
        }
        .box_support_page .nav-tabs> li> a, .box_support_page .nav-tabs> li> a:hover, .box_support_page .nav-tabs> li> a:focus, .box_support_page .nav-tabs> li.active> a, .box_support_page .nav-tabs> li.active> a:hover, .box_support_page .nav-tabs> li.active> a:focus {
            background: none;
            border: none;
            padding: 0;
            margin-bottom: 10px;
        }
        .nav-tabs>li.active>a, .nav-tabs>li.active>a:hover, .nav-tabs>li.active>a:focus {
            color: #555;
            background-color: #fff;
            border: 1px solid #ddd;
            border-bottom-color: transparent;
            cursor: default;
        }
        .nav-tabs>li>a {
            margin-right: 2px;
            line-height: 1.42857143;
            border: 1px solid transparent;
            border-radius: 4px 4px 0 0;
        }
    </style>
@endsection
@section('content')

    <div class="wrapper_main container view_pc" id="support_page">
        <!-- quang cáo -->

        <!-- breadcrumb  -->
        <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
          <li><i class="fa fa-chevron-right"></i><span>Câu hỏi thường gặp</span></li>
        </ul>
      </div> 
        <!-- breadcrumb  -->


        <!-- detail category -->
        <div class="col-md-12">
            <!-- content -->
            <div class="col-lg-2 col-md-3 col-sm-4">
                <div class="box_support_page row space_bottom_10">

                    <div class="block_item_content_support nav nav-tabs" role="tablist">
                        <ul class="nav nav-tabs" role="tablist">
                            <li><div class="title_support">Câu hỏi thường gặp</div></li>
                            <li data-rel="Tài khoản" role="presentation" class="cms_question"><a href="#tab_account" aria-controls="tab_account" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="false">Tài khoản</a></li>
                            <li data-rel="Đơn hàng tại Hasaki" role="presentation" class="cms_question"><a href="#tab_donhang" aria-controls="tab_donhang" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="false">Đơn hàng tại Hasaki</a></li>
                            <li data-rel="Đặt hàng tại Hasaki" role="presentation" class="cms_question"><a href="#tab_dathang" aria-controls="tab_dathang" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="false">Đặt hàng tại Hasaki</a></li>
                            <li data-rel="Phí vận chuyển" role="presentation" class=""><a href="#tab_phivanchuyen" aria-controls="tab_phivanchuyen" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="false">Phí vận chuyển</a></li>
                            <li data-rel="Hasaki Go" role="presentation" class="cms_support active"><a href="#tab_hasakigo" aria-controls="tab_hasakigo" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="true">Vận chuyển 2H</a></li>
                            <li data-rel="Dịch vụ Spa" role="presentation" class="cms_support"><a href="#tab_dichvuspa" aria-controls="tab_dichvuspa" role="tab" data-toggle="tab" class="item_content_support">Dịch vụ Spa</a></li>
                            <li> <div class="title_support">Thông tin hỗ trợ</div></li>
                            <li data-rel="Giới thiệu Hasaki" role="presentation" class="cms_support"><a href="#tab_gioithieu" aria-controls="tab_gioithieu" role="tab" data-toggle="tab" class="item_content_support">Giới thiệu Hasaki</a></li>
                            <li data-rel="Khách hàng thân thiết" role="presentation" class="cms_support"><a href="/tri-an-khach-hang" target="_blank" class="item_content_support">Khách hàng thân thiết</a></li>

                            <li data-rel="Hướng dẫn đặt hàng" role="presentation" class="cms_support"><a href="#tab_huongdandathang" aria-controls="tab_huongdandathang" role="tab" data-toggle="tab" class="item_content_support">Hướng dẫn đặt hàng</a></li>
                            <li data-rel="Hướng dẫn đặt hàng Hasaki Go" role="presentation" class="cms_support"><a href="#tab_huongdandat_hasakigo" aria-controls="tab_huongdandat_hasakigo" role="tab" data-toggle="tab" class="item_content_support">Hướng dẫn đặt hàng 2H</a></li>
                            <li data-rel="Hướng dẫn Thanh toán bằng VNPAY" role="presentation" class="cms_support">
                                <a href="#tab_huongdandat_vnpay_mobile" aria-controls="tab_huongdandat_vnpay_mobile" role="tab" data-toggle="tab" class="item_content_support">
                                    Hướng dẫn Thanh toán bằng VNPAY <img src="https://media.hasaki.vn/wysiwyg/Cover_categories/new_03.png" alt="" width="29" height="15" class="loading" data-was-processed="true"></a>
                            </li>
                            <li data-rel="Nhận hàng tại cửa hàng" role="presentation" class="cms_support"><a href="#tab_nhanhang_store" aria-controls="tab_nhanhang_store" role="tab" data-toggle="tab" class="item_content_support">Dịch vụ nhận hàng tại các chi nhánh Hasaki
                                    <img src="https://media.hasaki.vn/wysiwyg/Cover_categories/new_03.png" alt="" width="29" height="15" class="loading" data-was-processed="true"></a></li>
                            <li data-rel="Thẻ quà tặng Mobile gift" role="presentation" class="cms_support"><a href="#tab_mobile_gift" aria-controls="tab_mobile_gift" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="true">Thẻ quà tặng Mobile gift
                                    <img src="https://media.hasaki.vn/wysiwyg/Cover_categories/new_03.png" alt="" width="29" height="15" class="loading" data-was-processed="true"></a></li>
                            <li data-rel="Phiếu mua hàng Hasaki" role="presentation" class="cms_support"><a href="#tab_buyer_card" aria-controls="tab_buyer_card" role="tab" data-toggle="tab" class="item_content_support" aria-expanded="true">Phiếu mua hàng Hasaki
                                    <img src="https://media.hasaki.vn/wysiwyg/Cover_categories/new_03.png" alt="" width="29" height="15" class="loading" data-was-processed="true"></a></li>
                            <li data-rel="Quy trình giao hàng" role="presentation" class="cms_support"><a href="#tab_quytrinhgiaohang" aria-controls="tab_quytrinhgiaohang" role="tab" data-toggle="tab" class="item_content_support">Quy trình giao hàng</a></li>
                            <li data-rel="Điều khoản sử dụng" role="presentation" class="cms_support"><a href="#tab_dieukhoansudung" aria-controls="tab_dieukhoansudung" role="tab" data-toggle="tab" class="item_content_support">Điều khoản sử dụng</a></li>
                            <li data-rel="Chính sách đổi trả" role="presentation" class="cms_support"><a href="#tab_chinhsachdoitra" aria-controls="tab_chinhsachdoitra" role="tab" data-toggle="tab" class="item_content_support">Chính sách đổi trả</a></li>
                            <li data-rel="Chính sách bảo mật" role="presentation" class="cms_support"><a href="#tab_chinhsachbaomat" aria-controls="tab_chinhsachbaomat" role="tab" data-toggle="tab" class="item_content_support">Chính sách bảo mật</a></li>

                        </ul>
                        <!--<a href="#" class="btn btn_site_1">Gửi yêu cầu</a>-->
                        <script type="text/javascript">
                            $(document).ready(function(){
                                window.onload = function() {
                                    //$('a[href="#tab_gioithieu"]').trigger('click');// page thong tin ho tro
                                    var hash = window.location.hash.substring(1);
                                    if(hash != ''){
                                        jQuery('a[href="#'+hash+'"').trigger('click');
                                        var topp = jQuery('.title_support_page').offset().top;
                                        jQuery(document).scrollTop(topp);
                                    }
                                };

                                $('#support_page .block_item_content_support ul li').click(function(){
                                    var topp = jQuery('.title_support_page').offset().top;
                                    jQuery(document).scrollTop(topp);
                                    var isBlockQuestion = $(this).hasClass('cms_question');
                                    if(isBlockQuestion){
                                        $('._cms_block').html('Câu hỏi thường gặp');
                                    }
                                    var isSupport = $(this).hasClass('cms_support');
                                    if(isSupport){
                                        $('._cms_block').html('Thông tin hỗ trợ');
                                    }

                                    var _spRel = $(this).attr('data-rel');
                                    $('._title_cms').html(_spRel);
                                });
                            });
                        </script>
                    </div>

                </div>

            </div>
            <div class="col-lg-10 col-md-9 col-sm-8">
                    <div class="block_content_support row tab-content" id="accordion">
                        <div class="title_support_page"><span class="_cms_block">Câu hỏi thường gặp</span> » <span class="_title_cms">Tài khoản</span></div>

                        <div class="content_tab_left tab-pane active" id="tab_account" role="tabpanel">

                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_1" aria-expanded="false" aria-controls="taikhoan_fck_1" class="collapsed">Đăng ký thành viên Hasaki như thế nào?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="taikhoan_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="false" style="height: 0px;">

                                    <p>Quý khách vui lòng nhấn vào nút “<b>Đăng nhập/Đăng ký tài khoản</b>” trên góc phải màn hình sau đó chọn “<b>Đăng kí/Đăng ký ngay</b>” (Đối với Destop) hoặc tại góc trái màn hình, chọn biểu tượng Menu rồi chọn “<b>Đăng nhập/Đăng ký</b>” (Đối với Mobile). Vui lòng điền đầy đủ các thông tin được yêu cầu và nhấn nút “Đăng ký”. Hệ thống sẽ tự động gửi email thông báo về viêc kích hoạt tài khoản đến tài khoản email cá nhân của quý khách. Quý khách vui lòng click vào đường link để được xác nhận đã tạo tài khoản thành công.</p>
                                    <p>Trường hợp không nhận được email kích hoạt, quý khách vui lòng kiểm tra kỹ trong hộp thư rác hoặc Spam hoặc liên hệ trực tiếp qua Hotline để được hỗ trợ.
                                    </p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_2" aria-expanded="false" aria-controls="taikhoan_fck_2" class="collapsed">Tại sao tôi không thể đăng nhập vào tài khoản của tôi?</a></div>
                                <div id="taikhoan_fck_2" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">

                                    <p>Quý khách vui lòng kiểm tra kỹ về kiểu gõ hoặc phím Caps Look trong quá trình điền thông tin đăng nhập thành viên, trường hợp không thể đăng nhập thành công quý khách vui lòng chọn nút “quên mật khẩu” trên góc phải màn hình và nhập email đăng ký. Hệ thống sẽ tự động gửi một đường dẫn vào email của quý khách, quý khách vui lòng nhấp vào đường dẫn để tạo lại mật khẩu mới. Sau khi hoàn tất, quý khách có thể đăng nhập vào tài khoản bằng mật khẩu vừa tạo.</p>

                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_3" aria-expanded="false" aria-controls="taikhoan_fck_3" class="collapsed">Tôi muốn thay đổi thông tin tài khoản thành viên như thế nào?</a></div>
                                <div id="taikhoan_fck_3" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <p>Để thay đổi thông tin cá nhân quý khách vui lòng đăng nhập vào tài khoản của mình, chọn nút “Tài khoản của bạn ” rồi chọn vào nút “sửa” để thay đổi thông tin. </p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_4" aria-expanded="false" aria-controls="taikhoan_fck_4" class="collapsed">Tôi có thể sử dụng chung tài khoản với người khác được không?</a></div>
                                <div id="taikhoan_fck_4" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <p>Quý khách nên sử dụng tài khoản cá nhân để đảm bảo độ tin cậy cũng như quyền lợi của bản thân khi mua sắm. Việc sử dụng chung tài khoản có thể dẫn đến những sai sót mà người chịu ảnh hưởng trực tiếp chính là quý khách hàng. </p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_5" aria-expanded="false" aria-controls="taikhoan_fck_5" class="collapsed">Đăng kí thành viên tại Hasaki.vn sẽ giúp ích gì cho tôi?</a></div>
                                <div id="taikhoan_fck_5" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <p>Việc đăng kí tài khoản là cơ hội giúp bạn trở thành một trong những khách hàng thân thiết tại Hasaki.vn, được tiếp cận nhanh nhất các chương trình khuyến mãi, thông tin ưu đãi khi mua sắm.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#taikhoan_fck_6" aria-expanded="false" aria-controls="taikhoan_fck_6" class="collapsed">Hasaki có chương trình ưu đãi nào hấp dẫn dành cho khách hàng thân thiết?</a></div>
                                <div id="taikhoan_fck_6" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">
                                    <p>Hiện Hasaki.vn đang có chương trình ưu đãi nhằm tri ân các khách hàng thân thiết đã ủng hộ Hasaki trong thời gian vừa qua. Chương trình áp dụng cho các khách hàng đã mua sắm với tổng các hóa đơn tích đủ điểm 1.000.000đ trở lên. Cụ thể: Quý khách được giảm 5% khi mua mỹ phẩm, giảm 10% khi sử dụng các liệu trình làm đẹp tại Spa và đặc biệt khi tổng tích điểm các hóa đơn đạt  mốc 5.000.000đ quý khách còn được tặng thêm 1 lần trải nghiệm liệu trình Trẻ Hóa Da, Se Lỗ Chân Lông trị giá 350.000đ bằng công nghệ Revlite vô cùng hiện đại. Để biết thêm thông tin chi tiết vui lòng liên hệ Hotline: <b>1900 636 900.</b></p>
                                </div>
                            </div>
                        </div>

                        <div class="content_tab_left tab-pane" id="tab_donhang" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#donhang_fck_1" aria-expanded="true" aria-controls="donhang_fck_1">Tôi muốn kiểm tra lại đơn hàng đã mua?</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="donhang_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">

                                    <p>Quý khách bấm vào nút “<b>tài khoản</b>” trên góc phải màn hình sau đó chọn vào mục “<b>Tài khoản của bạn</b>” vào chọn vào ô “Đơn hàng của tôi” để kiểm tra lại các sản phẩm đã đặt mua</p>
                                    <p>Hoặc bạn có thể kiểm tra lại email Hasaki thông báo bạn đã đặt hàng thành công.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck"><a data-toggle="collapse" data-parent="#accordion" href="#collap_fck_2" aria-expanded="false" aria-controls="collap_fck_2" class="collapsed">Tôi muốn thay đổi hoặc hủy bỏ đơn hàng đã mua thì sao?</a></div>
                                <div id="collap_fck_2" class="fck_support_page panel-collapse collapse" role="tabpanel" aria-expanded="false">

                                    <p>Việc thay đổi sản phẩm trong đơn hàng quý khách vui lòng liên hệ Hasaki Care qua <b>Hotline</b> để được hướng dẫn chi tiết.</p>
                                    <p>Đơn hàng chỉ được hủy khi đơn hàng của quý khách chưa chuyển trạng thái cho đơn vị vận chuyển.</p>

                                </div>
                            </div>
                        </div>

                        <div class="content_tab_left tab-pane" id="tab_dathang" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dathang_fck_1" aria-expanded="true" aria-controls="dathang_fck_1">Tôi có thể đặt hàng qua điện thoại được không?</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="dathang_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">

                                    <p>Quý khách có thể liên hệ trực tiếp qua <b>Hotline</b> để được hướng dẫn đặt hàng, Hasaki luôn khuyến khích quý khách tạo tài khoản và đặt hàng online để được hưởng các chính sách ưu đãi thành viên tốt hơn.</p>
                                    <p>Hoặc bạn có thể kiểm tra lại email Hasaki thông báo bạn đã đặt hàng thành công.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dathang_fck_2" aria-expanded="false" aria-controls="dathang_fck_2" class="collapsed">Có giới hạn về số lượng sản phẩm khi đặt hàng không?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dathang_fck_2" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Quý khách có thể đặt hàng với số lượng sản phẩm tùy ý. Hasaki không giới hạn số lượng sản phẩm trong đơn hàng của quý khách.</p>
                                </div>
                            </div>

                        </div>
                        <div class="content_tab_left tab-pane" id="tab_huongdandat_hasakigo" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dathang_fck_3" aria-expanded="true" aria-controls="dathang_fck_3">Hướng dẫn đặt hàng với dịch vụ Giao hàng 2H</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="dathang_fck_3" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <p>Khách hàng khu vực nội thành Hồ Chí Minh sẽ có cơ hội trải nghiệm dịch vụ giao hàng 2H - dịch vụ giao hàng tốt nhất hiện nay tại Hasaki với mức phí ưu đãi. </p>
                                    <p>Các bước thực hiện để đặt thành công một đơn hàng:</p>
                                    <p><strong>1.Tìm kiếm sản phẩm</strong></p>
                                    <p>Tất cả các sản phẩm còn hàng tại chi nhánh 555 3 Tháng 2 đều được hỗ trợ giao hàng 2h. Quý khách có thể lựa chọn các cách sau để tìm kiếm sản phẩm mong muốn:</p>
                                    <p>a. Nhập từ khóa ở thanh tìm kiếm</p>
                                    <p>b. Tìm kiếm theo danh mục sản phẩm </p>
                                    <p>c. Tìm kiếm các sản phẩm ở các trang Hasaki deals, hàng bán chạy, hàng mới về, thương hiệu</p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/22.png"></div>

                                    <p><strong>2.Thêm vào giỏ hàng</strong></p>
                                    <p>Khi đã tìm được sản phẩm mong muốn, quý khách bấm vào hình hoặc tên sản phẩm để mở trang chi tiết sản phẩm. Sau đó:</p>
                                    <p>a.Đọc lại thông tin sản phẩm, hướng dẫn sử dụng, thông số sản phẩm</p>
                                    <p>b.Kiểm tra lại giá, sản phẩm quà tặng (nếu có), chọn số lượng</p>
                                    <p>c.Kiểm tra lại thời gian dự kiến nhận hàng, chọn “xem chi tiết” để hiểu rõ hơn về quy định của dịch vụ HasakiGo</p>
                                    <p>d.Chọn “Thêm vào giỏ hàng” (hoặc chọn “Mua ngay” để đến với bước Đặt hàng)</p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/26.png"></div>
                                    <p><strong>3.Kiểm tra giỏ hàng </strong></p>
                                    <p>a. Chọn biểu tượng giỏ hàng để kiểm tra lại các sản phẩm đã chọn, thêm giảm số lượng sản phẩm. (chọn “Cập nhật giỏ hàng” nếu có thay đổi)</p>
                                    <p>b. Chọn “Đặt hàng” để bắt đầu đặt hàng.</p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/27.png"></div>
                                    <p><strong>4.      Đặt hàng</strong></p>
                                    <p>a. Quý khách có thể chọn đăng nhập hoặc nhập số điện thoại, email.</p>
                                    <p>b. Nhập đầy đủ thông tin, ở mục địa chỉ giao hàng (chắc chắn rằng địa chỉ quý khách chọn thuộc khu vực được áp dụng dịch cụ giao hàng dưới 120 phút <a href="https://hasaki.vn/giao-hang-duoi-120-phut.html" target="_blank">https://hasaki.vn/giao-hang-duoi-120-phut.html )</a></p>
                                    <p>c. Quý khách phải chọn <strong>“Giao hàng dưới 120 phút”</strong> cho phương thức vận chuyển</p>
                                    <p>d. Lựa chọn phương thức thanh toán</p>
                                    <p>e. Chọn “Đặt hàng” để hoàn tất</p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/23.png"></div>
                                    <p><strong>5.    Kiểm tra và xác nhận đơn hàng</strong></p>
                                    <p>a. Bấm chọn mã đơn hàng để kiểm tra lại đơn hàng</p>
                                    <p>b. Lưu mã số để kiểm tra lại cho lần sau</p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/24.png"></div>
                                    <p><strong> 6. Theo dõi tình trạng đơn hàng</strong></p>
                                    <div class="text-center space_bottom_20"><img src="/images/support_center/25.png"></div>


                                </div>
                            </div>
                        </div>
                        <div class="content_tab_left tab-pane" id="tab_huongdandat_vnpay_mobile" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck">
                                    <a href="javascript:;">Hướng dẫn Thanh toán bằng VNPAY?</a>
                                </div>
                                <div class="fck_support_page panel-collapse collapse in" id="dathang_fck_3" role="tabpanel" aria-labelledby="">
                                    <p><strong>*&nbsp;&nbsp;&nbsp;</strong>Ngoài các hình thức thanh toán bằng tiền mặt khi nhận hàng (COD), Chuyển khoản ngân hàng, các loại voucher, Hasaki.vn đã tích hợp hình thức thanh toán VNPAY (Thanh toán bằng mobile banking) cho cả hệ thống online (đặt hàng tại website Hasaki.vn và offline (mua hàng trực tiếp tại các chi nhánh Hasaki)</p>
                                    <p><strong>*&nbsp;&nbsp;&nbsp;</strong>VNPAY QR là giải pháp thanh toán hiện đại do Công ty Cổ phần Giải pháp Thanh toán Việt Nam (VNPAY) phát triển. Khách hàng sử dụng tính năng QR Pay được tích hợp sẵn trên ứng dụng Mobile Banking của các ngân hàng liên kết (tìm trong mục menu của các ứng dụng này trên điện thoại), tiến hành quét mã QR Pay, nhập mã giảm giá (nếu có) và thanh toán trực tiếp từ tài khoản ngân hàng. Quý khách hàng lưu ý sử dụng đúng ứng dụng Mobile Banking của từng ngân hàng trên điện thoại </p>
                                    <div class="title_fck">
                                        <a href="javascript:;">Ngân hàng hỗ trợ thanh toán bằng VNPAY-QR</a>
                                    </div>
                                    <p>
                                    </p><div class="elementor-posts-container elementor-posts">
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_03.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_05.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_07.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_09.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_11.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/IVB.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/6.-Nam-Á-01.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/logo-ncb-200-x140.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_26.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_37.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/SCB-1.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_13.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/MSB-1.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_33.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/logo_mbbank.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/sacombank.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_35.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/Logo.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/viet-a-bank.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/shinhan-logo-1.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/250px-Logo_LienVietPostBank.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_22.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/saigonbank.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/seabank_iconapp.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/pvcombank.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/nhdpocb.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/HDBANK.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/hd_25.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/vietbank-200x140.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/ACB_Logo-1.png" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                        <article class="elementor-post elementor-grid-item post-3825 ngan_hang_lien_ket">
                                            <a class="elementor-post__thumbnail__link" href="#">
                                                <div class="elementor-post__thumbnail">
                                                    <img src="/img/vnpay/466x330.jpg" class="attachment-full size-full" alt="">
                                                </div>
                                            </a>
                                        </article>
                                    </div>
                                    <p></p>
                                    <div class="title_fck">
                                        <a href="javascript:;">Sử dụng vnpay như thế nào?</a>
                                    </div>
                                    <p><strong>*&nbsp;&nbsp;&nbsp;</strong>Ở màn hình thanh toán, chọn Phương thức thanh toán là Thanh toán qua VNPAY và Thanh toán bằng ứng dụng <b>MOBILE BANKING</b>  của các ngân hàng</p>
                                    <div class="elementor-row">
                                        <div class="elementor-image-box-wrapper elementor-col-25">
                                            <img width="300" height="498" src="/img/vnpay/buoc1.png" class="elementor-animation-hang attachment-full size-full" alt="">
                                            <div class="elementor-image-box-content">
                                                <h3 class="elementor-image-box-title">bước 1</h3>
                                                <p class="elementor-image-box-description">Tải và đăng nhập ứng dụng
                                                    <br><span class="mblue"><b>MOBILE BANKING</b></span></p>
                                            </div>
                                        </div>
                                        <div class="elementor-image-box-wrapper elementor-col-25">
                                            <img width="300" height="498" src="/img/vnpay/buoc2.png" class="elementor-animation-hang attachment-full size-full" alt="">
                                            <div class="elementor-image-box-content">
                                                <h3 class="elementor-image-box-title">bước 2</h3>
                                                <p class="elementor-image-box-description">Chọn tính năng <span class="mblue"><b>QR Pay </b></span><br>
                                                    tích hợp sẵn trong ứng dụng
                                                </p>
                                            </div>
                                        </div>
                                        <div class="elementor-image-box-wrapper elementor-col-25">
                                            <img width="300" height="498" src="/img/vnpay/buoc3.png" class="elementor-animation-hang attachment-full size-full" alt="">
                                            <div class="elementor-image-box-content">
                                                <h3 class="elementor-image-box-title">bước 3</h3>
                                                <p class="elementor-image-box-description">Quét mã<span class="vpay">vnpay</span><br>
                                                    để thanh toán
                                                </p>
                                            </div>
                                        </div>
                                        <div class="elementor-image-box-wrapper elementor-col-25">
                                            <img width="300" height="498" src="/img/vnpay/buoc4.png" class="elementor-animation-hang attachment-full size-full" alt="">
                                            <div class="elementor-image-box-content">
                                                <h3 class="elementor-image-box-title">bước 4</h3>
                                                <p class="elementor-image-box-description">Nhập số tiền  <br>&amp; xác minh giao dịch
                                                </p>
                                            </div>
                                        </div>
                                        <p class="txt_14"><strong>*&nbsp;&nbsp;&nbsp;</strong>Quý khách lưu ý: Với các đơn hàng thanh toán bằng VNPay không thành công, Quý khách có thể <strong>thanh toán lại</strong> đơn hàng từ trang Quản lý đơn hàng của Quý khách.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="content_tab_left tab-pane" id="tab_phivanchuyen" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck ">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#phivanchuyen_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1">Phí vận chuyển khi đặt hàng tại Hasaki.vn</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="phivanchuyen_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <p>
                                        Hasaki miễn phí vận chuyển tại <b>nội thành thành phố Hồ Chí Minh</b> cho các đơn hàng trên 90,000đ. Các đơn hàng dưới 90,000đ quý khách chỉ mất 10,000đ phí vận chuyển.
                                    </p>
                                    <p>
                                        Đối với những khách hàng thuộc <b>Huyện Nhà Bè</b> mức phí vận chuyển là 10.000đ. Miễn phí vận chuyển đối với đơn hàng từ 200.000đ trở lên.
                                    </p>

                                    <p>
                                        Đối với những khách hàng thuộc <b>khu vực khác</b>, quý khách có thể lựa chọn một trong hai hình thức thanh toán: chuyển khoản hoặc thanh toán trực tiếp khi nhận hàng. Lưu ý các đơn hàng có giá trị <b>trên 5.000.000đ</b>  chỉ được áp dụng hình thức chuyển khoản. Miễn phí vận chuyển đối với đơn hàng từ 700.000đ trở lên
                                    </p>
                                    <p>Cước phí vận chuyển cụ thể: (Áp dụng đối với trọng lượng đơn hàng từ 500g trở xuống)<br>
                                        &nbsp;  &nbsp; - Khu vực Miền Nam: 25.000đ<br>
                                        &nbsp;   &nbsp; - Khu vực Miền Bắc – Miền Trung: 40.000đ<br>
                                        &nbsp;   &nbsp; - Hà Nội: 30.000đ<br>
                                        &nbsp;   &nbsp; - Đà Nẵng: 35.000đ<br>
                                        &nbsp;   &nbsp; -	PhúQuốc: 40.000đ

                                    </p>
                                    <p><strong>Lưu ý:</strong> <br>
                                        Khu vực Miền Nam bao gồm: Cần Giờ, Củ Chi, Bình Chánh, Bình Phước, Bình Dương, Đồng Nai, Tây Ninh, Bà Rịa-Vũng Tàu, Long An, Đồng Tháp, Tiền Giang, An Giang, Bến Tre, Vĩnh Long, Trà Vinh, Hậu Giang, Kiên Giang ( không gồm Phú Quốc), Sóc Trăng, Bạc Liêu, Cà Mau, Cần Thơ.
                                        <!--                                    Khu vực Miền Nam bao gồm: Cần Giờ, Củ Chi, Bình Chánh, Hóc Môn (Xã Nhị Bình, Xã Tân Thới Nhì, Xã Xuân Thới Sơn, Xã Xuân Thới Thượng), Bình Phước, Bình Dương, Đồng Nai, Tây Ninh, Bà Rịa-Vũng Tàu, Long An, Đồng Tháp, Tiền Giang, An Giang, Bến Tre, Vĩnh Long, Trà Vinh, Hậu Giang, Kiên Giang ( không gồm Phú Quốc), Sóc Trăng, Bạc Liêu, Cà Mau, Cần Thơ.-->
                                    </p>
                                    <p>
                                        Số tài khoản ngân hàng:</p>
                                    <p>
                                        Tên công ty: Công ty TNHH Hasaki Beauty Và S.P.A<br>
                                        Số tài khoản: <b>060115453222</b> - Ngân hàng TM CP Sài Gòn Thương Tín (Sacombank)- PGD LĂng Cha Cả - Tp.HCM</p>
                                    <p>
                                        Tên công ty: Công ty TNHH Hasaki Beauty Và S.P.A<br>
                                        Số tài khoản: <b>118002658699</b> - Ngân hàng TM CP Công Thương Việt Nam (Vietinbank) - Chi nhánh 3 - Tp.HCM</p>
                                    <p>
                                        Tên công ty: Công ty TNHH Hasaki Beauty Và S.P.A<br>
                                        Số tài khoản: <b>1606201047880</b> - Ngân hàng NN và PTNN Việt Nam (Agribank) - Chi nhánh An Phú - Tp.HCM</p>
                                    <p>
                                        Tên công ty: Công ty TNHH Hasaki Beauty Và S.P.A<br>
                                        Số tài khoản: <b>0071001109681</b> - Ngân hàng TMCP Ngoại thương Việt Nam - CN Tp.HCM
                                    </p>

                                    <p>Mọi thắc mắc khác không nằm trong bảng câu hỏi thường gặp quý khách vui lòng liên hệ <b>1900.636.900</b> để được hỗ trợ trực tiếp.</p>
                                </div>
                            </div>


                        </div>

                        <div class="content_tab_left tab-pane" id="tab_huongdandathang" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#huongdandathang_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1">Hướng dẫn đặt hàng</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="huongdandathang_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">

                                    <p>Quý khách có thể đặt hàng trực tuyến ở Website <strong><a href="https://hasaki.vn">Hasaki.vn</a></strong> thông qua các bước đặt hàng cơ bản.</p>
                                    <p><b>1. Tìm kiếm sản phẩm:</b></p>
                                    <p>Quý khách có thể tìm sản phẩm theo 2 cách:</p>
                                    <ul>
                                        <li><p>a. Gõ tên sản phẩm vào thanh tìm kiếm.</p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang1.jpg"></div>
                                        </li>

                                        <li><p>b. Tìm theo danh mục sản phẩm; thương hiệu sản phẩm hoặc có thể tham khảo qua những sản phẩm hot, những sản phẩm đang bán chạy nhất tại <strong><a href="https://hasaki.vn">Hasaki.vn</a></strong></p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang2.jpg"></div>
                                        </li>
                                    </ul>
                                    <p><b>2. Đặt hàng:</b></p>
                                    <p>Khi đã tìm được sản phẩm mong muốn, quý khách vui lòng bấm vào hình hoặc tên sản phẩm để vào được trang thông tin chi tiết của sản phẩm.</p>
                                    <p>Nếu quý khách chỉ muốn mua 1 sản phẩm vừa chọn thì click vào ô “<b>mua ngay</b>” sau đó làm theo hướng dẫn trên Website.
                                    </p><div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/muangay.jpg"></div>
                                    <p></p>
                                    <p>Để đặt nhiều sản phẩm khác nhau vào cùng 1 đơn hàng, quý khách vui lòng thực hiện theo các bước sau: </p>
                                    <ul>
                                        <li><p>a.	Quý khách sẽ bấm vào ô “ <b>thêm vào giỏ hàng</b>” và tiếp tục tìm thêm các sản phẩm khác.</p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang3.jpg"></div>
                                        </li>
                                        <li><p>b.	Sau khi đã cho các sản phẩm cần mua vào giỏ hàng, quý khách vui lòng bấm vào nút “<b>giỏ hàng</b>” bên góc phải màn hình để xem lại sản phẩm đã chọn.</p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang4.jpg"></div>
                                        </li>
                                        <li><p>c.	Trong giỏ hàng của quý khách bên góc trái có nút tiếp tục mua hàng để quý khách chọn nếu như muốn mua thêm sản phẩm khác.</p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang5.jpg"></div>
                                        </li>
                                        <li><p>d.	Sau khi đã chọn được các sản phẩm cần mua vào giỏ hàng, quý khách vui lòng bấm nút “<b>Tiến hành đặt hàng</b>” bên góc phải màn hình.</p>
                                            <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang6.jpg"></div>
                                        </li>
                                    </ul>

                                    <p><b>3. Đăng nhập hoặc đăng ký tài khoản:</b></p>
                                    <p>Quý khách vui lòng đăng nhập bằng tài khoản đã có ở <strong><a href="https://hasaki.vn">Hasaki.vn</a></strong> hoặc đăng nhập thông qua Facebook hoặc tài khoản Google. Trong trường hợp chưa đăng ký tài khoản, quý khách có thể chọn dòng " <b>Đăng ký ngay</b>" và bắt đầu nhập địa chỉ email, mật khẩu tùy ý để đăng ký tài khoản.</p>
                                    <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang7.jpg"></div>
                                    <p>Quý khách có thể đặt hàng mà không cần đăng nhập nhưng quý khách lưu ý phải điền đầy đủ và chính xác về thông tin nhận hàng, đặc biệt là địa chỉ mail và số điện thoại để Hasaki xác nhận đơn hàng.</p>
                                    <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang8.jpg"></div>
                                    <p>Sau khi đã hoàn tất các bước trên, quý khách vui lòng bấm "<b>Tiếp Tục</b>" để đến bước tiếp theo:</p>

                                    <p><b>4. Điền địa chỉ nhận hàng:</b></p>
                                    <p>Quý khách điền địa chỉ nhận hàng theo như trên trang hướng dẫn. Trường hợp quý khách có nhiều địa chỉ để nhận hàng thì quý khách lưu ý địa chỉ nào nằm trong ô “mặc định” đầu tiên bên trái sẽ là địa chỉ Hasaki lên hệ để giao hàng.</p>
                                    <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang9.jpg"></div>

                                    <p><b>5. Chọn phương thức thanh toán:</b></p>
                                    <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang10.jpg"></div>
                                    <p>Nếu các thông tin trên đã chính xác, quý khách vui lòng bấm "<b>Đặt hàng</b>", hệ thống sẽ bắt đầu tiến hành tạo đơn hàng dựa trên các thông tin quý khách đã đăng ký.</p>
                                    <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/dathang11.jpg"></div>

                                    <p>Quý khách có thể tham khảo các phương thức thanh toán sau đây và lựa chọn áp dụng phương thức phù hợp:</p>
                                    <p><b>Cách 1:</b> Thanh toán sau khi nhận hàng (COD): Quý khách sẽ thanh toán lúc nhận được sản phẩm từ nhân viên giao nhận hoặc nhân viên chuyển phát tại địa chỉ khách hàng đã đăng ký.</p>
                                    <p><b>Cách 2:</b> Chọn hình thức chuyển khoản và thực hiện chuyển khoản theo mẫu:
                                        <span class="ma_donhang" style="font-size: 16px;">Thanh toán đơn hàng số: mã đơn hàng, số điện thoại.</span>
                                    </p>
                                    <p><b>Lưu ý</b> các đơn hàng có giá trị trên <b>5.000.000đ</b> chỉ được áp dụng hình thức chuyển khoản.</p>
                                    <p><b>6. Kiểm tra và xác nhận đơn hàng</b></p>
                                    <p>Sau khi hoàn thành tất cả bước đặt mua, hệ thống sẽ gửi đến quý khách mã số đơn hàng để kiểm tra theo dõi tình trạng đơn hàng.</p>
                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_quytrinhgiaohang" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#quytrinhgiaohang_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1">Quy trình giao hàng:</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="quytrinhgiaohang_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">

                                    <p>a.	Hasaki liên lạc với bạn để thống nhất thời gian giao hàng sẽ giao sản phẩm đến địa điểm mà bạn đã cung cấp trong đơn đặt hàng. Hasaki sẽ cố gắng <b>giao hàng trong thời gian từ 24h đến 48h giờ</b> làm kể từ lúc quý khách đặt hàng. Tuy nhiên, vẫn có những sự chậm trễ do nguyên nhân khách quan (lễ, tết, địa chỉ nhận hàng khó tìm, sự chậm trễ từ dịch vụ chuyển phát…), rất mong bạn có thể thông cảm vì những lý do ngoài sự chi phối của chúng tôi.</p>
                                    <p>b.	Nếu quý khách không thể có mặt trong đợt nhận hàng thứ nhất, Hasaki sẽ liên lạc lại với quý khách để sắp xếp thời gian giao hàng hoặc hướng dẫn bạn đến công ty vận chuyển để nhận hàng.</p>
                                    <p>c.	Nếu việc giao và nhận hàng không thành công do không thể liên lạc được với quý khách trong vòng 3 ngày, chúng tôi sẽ thông báo đến bạn về việc hủy đơn hàng và hoàn trả các chi phí mà bạn đã thanh toán trong vòng 30 ngày.</p>

                                    <p>d.	Hasaki sẽ báo ngay đến bạn nếu có sự chậm trễ khi giao hàng, nhưng trong phạm vi pháp luật cho phép, chúng tôi sẽ không chịu trách nhiệm cho bất cứ tổn thất nào, các khoản nợ, thiệt hại hoặc chi phí phát sinh từ việc giao hàng trễ.</p>
                                    <p>e.	Hasaki lưu ý với bạn rằng có một số địa điểm mà dịch vụ chuyển phát không thể giao hàng được. Khi đó, Hasaki sẽ thông báo đến bạn qua thông tin liên lạc mà bạn đã cung cấp khi đặt hàng. Chúng tôi có thể sắp xếp giao hàng đến một địa chỉ khác thuận tiện hơn hoặc tiến hành hủy đơn hàng.</p>
                                    <p>f.	Khi nhận sản phẩm, quý khách vui lòng kiểm tra kỹ sản phẩm trước khi ký nhận hàng hóa. Bạn nên giữ lại biên lai mua hàng để làm bằng chứng trong trường hợp muốn liên hệ lại về sản phẩm đã mua.</p>
                                    <p>g.	Quý khách nên cẩn thận khi sử dụng vật sắc nhọn để mở sản phẩm vì bạn có thể làm hỏng sản phẩm. Hasaki không chịu bất cứ rủi ro, tổn thất, hư hại về sản phẩm sau khi bạn đã kiểm tra kỹ lưỡng và ký nhận sản phẩm.</p>
                                    <p>h.	Sản phẩm được đóng gói theo tiêu chuẩn đóng gói của Hasaki. Nếu bạn có nhu cầu đóng gói đặc biệt khác, vui lòng cộng thêm phí phát sinh.</p>
                                    <p>i.   Trong trường hợp những đơn hàng đã xác nhận của quý khách được đặt ở những ngày gần nhau, <a href="https://hasaki.vn">Hasaki.vn</a> sẽ cố gắng bổ sung vào đơn hàng và giao chung một lần cho quý khách.</p>
                                    <p>j.   Mọi thông tin về việc thay đổi hay hủy bỏ đơn hàng đề nghị quý khách thông báo sớm để <a href="https://hasaki.vn">Hasaki.vn</a> có thể hủy hoặc chỉnhh sửa đơn hàng cho bạn.</p>
                                    <p>k.   Chỉ nhận đổi trả sản phẩm khi lỗi đến từ nhà sản xuất hoặc bị hư hỏng trong quá trình vận chuyển với điều kiện không quá 3 ngày sau khi giao hàng.</p>
                                    <p>l.  Hasaki.vn nhận giao sản phẩm đến tận tay khách hàng và thanh toán khi nhận hàng hoặc quý khách hàng có thể chọn hình thức chuyển khoản trước (nếu muốn). <b>Lưu ý</b> các đơn hàng có giá trị trên 5.000.000đ chỉ được áp dụng hình thức chuyển khoản.</p>
                                    <p>m. Đối với những đơn hàng có sản phẩm Pre-Oder (đặt hàng trước), Hasaki sẽ tiến hành giao sau khi các sản phẩm Pre-Oder đã về hàng.</p>
                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_chinhsachdoitra" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#chinhsachdoitra_fck_1" aria-expanded="true" aria-controls="chinhsachdoitra_fck_1">Chính sách đổi trả:</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="chinhsachdoitra_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <table cellpadding="0" cellspacing="0" border="1" bordercolor="#cccccc" width="100%" class="space_bottom_20" style="text-align:center">
                                        <tbody><tr>
                                            <td>&nbsp;</td>
                                            <td style="font-weight:700">Sản Phẩm Lỗi (Từ phía nhà cung cấp)</td>
                                            <td style="font-weight:700">Sản Phẩm Lỗi(Từ phía người sử dụng)</td>
                                            <td style="font-weight:700">Sản Phẩm Không Lỗi</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:700">1 – 14 ngày</td>
                                            <td>Đổi mới Trả không thu phí </td>
                                            <td>Không hỗ trợ đổi trả </td>
                                            <td>Đổi mới</td>
                                        </tr>
                                        <tr>
                                            <td style="font-weight:700">15 ngày trở đi</td>
                                            <td colspan="3" style="text-align:center">Không hỗ trợ đổi trả</td>
                                        </tr>
                                        </tbody></table>
                                    <p style="font-size:17px; font-weight:700;">QUY ĐỊNH ĐỔI TRẢ SẢN PHẨM:</p>
                                    <p><b>1.	Các trường hợp nhận đổi trả:</b></p>
                                    <p>- Sản phẩm mắc lỗi từ phía nhà sản xuất (hỏng hóc, đổ vỡ sản phẩm, bị lỗi kĩ thuật…).</p>
                                    <p>- Sản phẩm bị hư hỏng, trầy xước do quá trình vận chuyển của nhân viên giao hàng.</p>
                                    <p>- Sản phẩm đã hết hoặc gần hết thời hạn sử dụng.</p>
                                    <p>- Sản phẩm không đúng như yêu cầu của khách hàng  do Hasaki soạn sai sản phẩm hoặc lấy nhầm tông màu , loại sản phẩm.</p>
                                    <p>- Sản phẩm còn nguyên vỏ hộp, tem nhãn và chưa qua sử dụng.</p>
                                    <p><b>2.	Các trường hợp không áp dụng đổi trả.</b></p>
                                    <p>- Sản phẩm quà tặng, các sảm phẩm trong chương trình giảm giá đặc biệt.</p>
                                    <p>- Sản phẩm đã quá hạn đổi trả (14 ngày).</p>
                                    <p>- Sản phẩm đã bị bóc tem nhãn, seal nếu có.</p>
                                    <p>- Sản phẩm khách đã thử hoặc qua sử dụng từ 1 lần trở lên.</p>
                                    <p>- Bao bì, vỏ hộp sản phẩm bị hư hỏng, trầy xước do lỗi từ phía khách hàng.</p>
                                    <p>- Sản phẩm không phải mua từ bên Hasaki.</p>
                                    <p><b>3.	Cách thức đổi trả:</b></p>
                                    <p>- Hasaki.vn nhận đổi trả sản phẩm cho khách hàng trong vòng 14 ngày , tính kể từ ngày khách hàng mua hoặc nhận được sản phẩm từ bên giao hàng.</p>
                                    <p>- Khách hàng cần phải thông báo cho nhân viên Hasaki.vn lí do đổi trả và địa chỉ, số điện thoại liên lạc chính xác để Hasaki.vn có thể thực hiện quy trình đổi trả sản phẩm một cách nhanh chóng nhất theo yêu cầu của quý khách.</p>
                                    <p>- Hasaki.vn quan tâm đến sự hài lòng của khách hàng và mong muốn nâng cao chất lượng dịch vụ, Hasaki.vn nhận đổi trả sản phẩm miễn phí cho khách hàng theo đúng quy định nêu trên . Khách ở khu vực Hồ Chí Minh , Hasaki khuyến khích khách đến Showroom để nhân viên có thể check lại sản phẩm và khách có thể xem &amp; lựa chọn đổi sản phẩm đúng theo ý của quý khách. Với các khách tỉnh, Quý khách có thể chuyển hàng qua bưu điện &amp; Liên hệ với Hasaki về sản phẩm đổi, mã bưu điện,… để Hasaki có thể xử lý và gửi hàng lại sớm nhất khi nhận được sản phẩm.</p>


                                    <p><b>6. Thông tin liên hệ và Trung tâm dịch vụ khách hàng:</b></p>
                                    <p>Mọi thắc mắc và ý kiến đóng góp vui lòng liên hệ:</p>
                                    CN 1 : 71 Hoàng Hoa Thám, P.13, Q.Tân Bình.<br>CN 2 : 555 Đường 3 Tháng 2, P.8, Q.10.<br>CN 3 : 176 Phan Đăng Lưu, P.3, Q.Phú Nhuận.<br>CN 4 : 94 Lê Văn Việt, P.Hiệp Phú, Q.9.<br>CN 5 : 119 Nguyễn Gia Trí (D2 cũ), P.25, Q.Bình Thạnh<br>CN 6 : 657B Quang Trung, P.11, Q.Gò Vấp<br>CN 7 : 468A Nguyễn Thị Thập, P.Tân Quy, Q.7<br>CN 8 : 447 Phan Văn Trị Phường 5, Q.Gò Vấp<br>CN 9 : 6M-6M1 Nguyễn Ảnh Thủ, P.Trung Mỹ Tây, Quận 12<br>                                Hotline: <b>1900 636 900</b>
                                    <p></p>
                                </div>
                            </div>

                        </div>
                        <div class="content_tab_left tab-pane" id="tab_gioithieu" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#quytrinhgiaohang_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1"></a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="quytrinhgiaohang_fck_1" role="tabpanel" aria-labelledby="">
                                    <div class="text-center space_bottom_10"><img src="/images/graphics/logo_180x40.svg" alt="Hasaki - Chất lượng cho tất cả" height="150px" width="300px"></div>

                                    <p><b>Quá trình hình thành:</b></p>
                                    <p><strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> được thành lập tại Việt Nam vào tháng 4/2016 với mục tiêu chăm sóc sắc đẹp sức khỏe toàn diện cho người Việt Nam, <strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> đã tạo ra những trải nghiệm mua sắm trực tuyến tuyệt vời cùng dịch vụ chăm sóc Spa chuyên nghiệp với các thiết bị hiện đại hàng đầu thế giới hiện nay.</p>
                                    <p><strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> cam kết cung cấp các sản phẩm chính hãng và nhập khẩu 100% từ Mỹ, Pháp… Số lượng sản phẩm và dịch vụ lớn nhất với chủng loại đa dạng, phong phú sẽ đáp ứng tất cả nhu cầu mua sắm của bạn.</p>

                                    <p><strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> cam kết tư vấn và hỗ trợ quý khách kịp thời, nhiệt tình theo đúng các quy định của pháp luật và chuẩn mực đạo đức nghề nghiệp.</p>
                                    <p><strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> cam kết bảo mật thông tin khách hàng.</p>
                                    <p><strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> cam kết vận hành tốc độ giao hàng nhanh vượt trội nhằm rút ngắn tối đa thời gian khách hàng nhận được sản phẩm.</p>
                                    <p>Đến với <strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> khách hàng sẽ trải nghiệm việc mua sắm trực tuyến với các bước thanh toán an toàn, đơn giản, nhanh chóng, đáp ứng tiêu chuẩn Quốc tế.</p>
                                    <p>“ <b class="txt_16">Hasaki quality for all - chất lượng cho tất cả</b>” <strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> luôn nỗ lực không ngừng nhằm nâng cao chất lượng dịch vụ để khách hàng được hưởng các dịch vụ chăm sóc tốt nhất.</p>
                                    <p class="text-center">Thông tin chi tiết vui lòng liên hệ:</p>
                                    <p class="text-center txt_16"><b>CÔNG TY TNHH HASAKI BEAUTY &amp; SPA</b></p>
                                    <div class="row">
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 1:<br> <strong class="txt_14">71 Hoàng Hoa Thám, Phường 13, Quận Tân Bình</strong></p>
                                            <img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/71-Hoang-Hoa-Tham.jpg" alt="">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 2:<br> <strong class="txt_14">555 đường 3 tháng 2, phường 8, quận 10</strong></p>
                                            <img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/555-bathanghai.jpg" alt="">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 3:<br> <strong class="txt_14"> 176 Phan Đăng Lưu, Phường 3, Quận Phú Nhuận</strong></p>
                                            <img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/176-Phandangluu.jpg" alt="">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 4:<br> <strong class="txt_14"> 94 Lê Văn Việt, Phường Hiệp Phú, Quận 9</strong></p>
                                            <img title="" src="/images/graphics/cuahang4.JPG" alt="">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 5:<br> <strong class="txt_14"> 119 Nguyễn Gia Trí (D2 cũ), P.25, Q.Bình Thạnh</strong></p>
                                            <img title="" src="/images/graphics/cuahang-5.jpg" alt="">
                                        </div>
                                        <div class="col-lg-4 col-md-4 col-sm-4 space_bottom_15">
                                            <p class="text-center">CN 6:<br> <strong class="txt_14"> 657B Quang Trung, P.11, Q.Gò Vấp</strong></p>
                                            <img title="" src="/images/graphics/cuahang6.jpg" alt="">
                                        </div>
                                    </div>

                                    <div class="space_bottom_10">&nbsp;</div>
                                    <p>Mã số thuế: <b>0313612829</b></p>
                                    <p>Điện thoại: <b>1900 636 900</b>  </p>
                                    <p>Email:<a href="email:hotro@hasaki.vn" class="txt_color_1">hotro@hasaki.vn</a> </p>
                                    <p>Fanpage: <a href="https://www.facebook.com/Hasaki.vn/" target="_blank">https://www.facebook.com/Hasaki.vn/</a></p>


                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_dieukhoansudung" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#quytrinhgiaohang_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1">Điều khoản sử dụng</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="quytrinhgiaohang_fck_1" role="tabpanel" aria-labelledby="">

                                    <p><b>1. Giới thiệu</b></p>
                                    <p>Chúng tôi là Công ty TNHH Hasaki Beauty &amp; SPA có cơ sở 1 tại số 71 Hoàng Hoa Thám, Phường 13, Quận Tân Bình, Tp.Hồ Chí Minh và cơ sở 2 tại 555 đường 3 tháng 2, phường 8, quận 10 và CN 3: 176 Phan Đăng Lưu, Phường 3, Quận Phú Nhuận và CN4 tại 94 Lê Văn Việt, Phường Hiệp Phú, Quận 9, thành lập Sàn giao dịch thương mại điện tử thông qua website <a href="https://hasaki.vn/">www.hasaki.vn</a> và đã được đăng ký chính thức với Bộ Công Thương Việt Nam.</p>
                                    <p><b>2. Quy định sử dụng</b></p>

                                    <p>. Khi khách hàng vào Website <a href="https://hasaki.vn">Hasaki.vn</a> với tư cách khách viếng thăm hay thành viên đã đăng ký, xin vui lòng đọc kỹ quy định sử dụng. Hasaki có quyền thay đổi, chỉnh sửa, thêm hoặc lược bỏ bất kỳ phần nào trong Điều khoản sử dụng này, vào bất cứ lúc nào. Các thay đổi có hiệu lực ngay khi được đăng trên Website <a href="https://hasaki.vn">Hasaki.vn</a> mà không cần thông báo trước và quý khách có trách nhiêm phải cập nhật Website <a href="https://hasaki.vn">Hasaki.vn</a> thường xuyên để theo dõi các thông tin được đăng để có thể cập nhật kịp thời. Việc bạn tiếp tục sử dụng trang <a href="https://hasaki.vn">Hasaki.vn</a> sau khi những thay đổi đó được đăng đồng nghĩa với việc bạn đã đồng ý với những sửa đổi trong Quy định sử dụng</p>
                                    <p>Hasaki không chịu trách nhiệm về chất lượng đường truyền Internet ảnh hưởng đến tốc độ truy cập của bạn vào Website <b><a href="https://hasaki.vn">Hasaki.vn</a></b>.</p>
                                    <p><b>3. Quy định và hướng dẫn liên quan đến khách hàng:</b></p>
                                    <p>Khách hàng phải đảm bảo đủ 18 tuổi, hoặc truy cập dưới sự giám sát của cha mẹ hay người giám hộ hợp pháp. Khách hàng đảm bảo có đầy đủ hành vi dân sự để thực hiện các giao dịch mua bán hàng hóa theo quy định hiện hành của pháp luật Việt Nam.</p>
                                    <p>Hasaki có quyền hỏi thêm về thông tin khách hàng khi xác nhận đơn hàng để đảm bảo mọi thông tin liên lạc về đơn hàng được xử lý chính xác nhất.</p>
                                    <p>Hasaki sẽ cấp một tài khoản sử dụng để khách hàng có thể mua sắm trong khuôn khổ Điều khoản và Điều kiện sử dụng đã đề ra. Quý khách hàng sẽ phải đăng ký tài khoản với thông tin xác thực về bản thân và phải cập nhật nếu có bất kỳ thay đổi nào. Mỗi người truy cập phải có trách nhiệm với mật khẩu, tài khoản và hoạt động của mình trên <a href="https://hasaki.vn">Hasaki.vn</a>. Ngoài ra, quý khách hàng phải thông báo cho Hasaki biết khi tài khoản bị truy cập trái phép Hasaki không chịu bất kỳ trách nhiệm nào, dù trực tiếp hay gián tiếp, đối với những thiệt hại hoặc mất mát gây ra do Quý khách không tuân thủ quy định.</p>
                                    <p>Nghiêm cấm sử dụng bất kỳ phần nào của trang <a href="https://hasaki.vn">Hasaki.vn</a> với mục đích thương mại hoặc nhân danh bất kỳ đối tác thứ ba nào nếu không được Hasaki cho phép bằng văn bản. Nếu vi phạm bất cứ điều nào trong đây, Hasaki sẽ hủy tài khoản của khách mà không cần báo trước.</p>
                                    <p>Trong suốt quá trình đăng ký, Quý khách sẽ nhận email quảng cáo từ Website <a href="https://hasaki.vn">Hasaki.vn</a>. Nếu không muốn tiếp tục nhận mail, quý khách có thể từ chối bằng cách nhấp vào đường link ở dưới cùng trong mọi email quảng cáo.</p>
                                    <p>Tất cả nội dung tại Website <a href="https://hasaki.vn">Hasaki.vn</a> và ý kiến phê bình của Quý khách đều là tài sản của chúng tôi. Nếu chúng tôi phát hiện bất kỳ thông tin giả mạo nào, chúng tôi sẽ khóa tài khoản của quý khách ngay lập tức hoặc áp dụng các biện pháp khác theo quy định của pháp luật Việt Nam.</p>
                                    <p><b>4. Chấp nhận đơn hàng, giá cả và điều khoản bất khả kháng:</b></p>
                                    <p>Hasaki luôn luôn nỗ lực để đảm bảo rằng mọi thông tin và tài liệu trên website <a href="https://hasaki.vn">Hasaki.vn</a> là chính xác. Tuy nhiên, không có đảm bảo hay mô tả nào, dù là được nêu rõ hay ngụ ý, được đưa ra để chắc chắn rằng mọi thông tin và tài liệu là hoàn thiện, chính xác, mới nhất, phù hợp cho mục đích cụ thể và, trong phạm vi cho phép.</p>
                                    <ul>
                                        <li>a.	Đôi lúc vẫn có sai sót xảy ra tùy theo từng trường hợp Hasaki sẽ liên hệ hướng dẫn hoặc thông báo hủy đơn hàng đó cho quý khách. Hasaki cũng có quyền từ chối hoặc hủy bỏ bất kỳ đơn hàng nào dù đơn hàng đó chưa hay đã được xác nhận hoặc đã thanh toán. Không bên nào phải chịu trách nhiệm cho bên nào cả khi đơn hàng bị hủy, ngoại trừ việc Hasaki có trách nhiệm hoàn trả tiền nếu bạn đã thanh toán mà chưa nhận được sản phẩm.</li>
                                        <li>b.	Hasaki sẽ không chịu trách nhiệm nếu quy trình mua hàng không diễn ra đúng cam kết do những nguyên nhân bất khả kháng như tai nạn, hiểm họa thiên nhiên, hành động của bên thứ ba (bao gồm và không giới hạn trong tin tặc, các nhà cung cấp, chính phủ, thuộc chính phủ, cơ quan đa quốc gia hay các chính quyền địa phương), bạo loạn, chiến tranh, trường hợp quốc gia khẩn cấp, khủng bô, dịch bệnh, hỏa hoạn, bão lụt, sự cố kỹ thuật của bên thứ ba, gián đoạn của tiện ích công cộng (bao gồm điện, viễn thông, hay Internet), thiếu hụt hoặc không có khả năng để lấy sản phẩm, vật liệu, thiết bị, hay vận chuyển trong điều kiện bất khả kháng.</li>
                                    </ul>
                                    <p><b>6. Thay đổi hoặc hủy bỏ giao dịch:</b></p>
                                    <p>Khách hàng có trách nhiệm cung cấp thông tin đầy đủ và chính xác khi tham gia giao dịch tại Website <a href="https://hasaki.vn">Hasaki.vn</a>. Trong trường hợp khách hàng nhập sai thông tin Hasaki có quyền từ chối thực hiện giao dịch. Ngoài ra, trong mọi trường hợp, khách hàng đều có quyền đơn phương chấm dứt giao dịch bằng cách thông báo cho Hasaki về việc hủy giao dịch qua đường dây nóng (hotline) hoặc gửi yêu cầu qua địa chỉ mail:</p>
                                    <p>Trong trường hợp bạn muốn khiếu nại về sản phẩm Hasaki mong bạn có thể hợp tác bằng cách cung cấp thông tin càng chi tiết càng tốt về tình trạng sản phẩm và các thông tin liên quan đến việc mua hàng như mã đơn hàng, xác nhận đơn hàng, hóa đơn mua hàng.</p>
                                    <p>Quý khách có thể tham khảo thêm về quy định đổi trả hàng qua Chính sách đổi trả để Hasaki có thể phục vụ khách hàng tốt nhất.</p>
                                    <p><b>7. Quảng cáo trên trang <a href="https://hasaki.vn">Hasaki.vn</a>:</b></p>
                                    <p>Hasaki luôn tuân theo các quy định về trang website do Cục Quản lý Tiêu chuẩn Quảng Cáo quy định.</p>
                                    <p><b>8. Thông báo:</b></p>
                                    <p>Mọi thông báo liên quan đến việc mua hàng sẽ được gửi văn bản đến bạn qua email hoặc bằng điện thoại cho khách hàng.</p>
                                    <p><b>9. Thương hiệu và bản quyền sử hữu trí tuệ:</b></p>
                                    <p>Mọi quyền sở hữu trí tuệ (đã đăng ký hoặc chưa đăng ký), nội dung thông tin và tất cả các logo, biểu tượng thiết kế, văn bản, đồ họa, phần mềm, hình ảnh, video, âm nhạc, âm thanh, biên dịch phần mềm, mã nguồn và phần mềm cơ bản đều là tài sản của Hasaki. Toàn bộ nội dung của trang <a href="https://hasaki.vn">Hasaki.vn</a> được bảo vệ bởi luật bản quyền của Việt Nam và các công ước quốc tế. Bản quyền đã được bảo lưu.</p>
                                    <p>Bạn không được sử dụng bất kỳ tài liệu nào trên trang Website <a href="https://hasaki.vn">Hasaki.vn</a> cho mục đích thương mại mà không có sự cho phép từ chúng tôi hay những người đã cấp phép cho chúng tôi. Nếu bạn in, sao chép, hoặc tải bất kỳ phần nào của trang <a href="https://hasaki.vn">Hasaki.vn</a> và vi phạm Quy định Sử dụng, bạn sẽ phải hoàn trả hoặc hủy bỏ các bản sao của các tài liệu mà bạn đã làm ra.</p>
                                    <p><b>10. Giải quyết tranh chấp:</b></p>
                                    <p>Bất kỳ tranh cãi, khiếu nại hoặc tranh chấp phát sinh từ hoặc liên quan đến giao dịch tại <b><a href="https://hasaki.vn">Hasaki.vn</a></b> hoặc các Quy định và Điều kiện này đều sẽ được giải quyết bằng hình thức thương lượng, hòa giải, trọng tài và/hoặc Tòa án theo Luật bảo vệ Người tiêu dùng về Giải quyết tranh chấp giữa người tiêu dùng và tổ chức, cá nhân kinh doanh hàng hóa, dịch vụ.</p>
                                    <p><b>11. Luật pháp và thẩm quyền tại Lãnh thổ Việt Nam:</b></p>
                                    <p>Tất cả các Điều Khoản và Điều Kiện này và Hợp Đồng (và tất cả nghĩa vụ phát sinh ngoài hợp đồng hoặc có liên quan) sẽ bị chi phối và được hiểu theo luật pháp của Việt Nam. Nếu có tranh chấp phát sinh bởi các Quy định Sử dụng này, quý khách hàng có quyền gửi khiếu nại/khiếu kiện lên Tòa án có thẩm quyền tại Việt Nam để giải quyết.</p>
                                    <p><b>12. Ngoại lệ:</b></p>
                                    <p>Nếu có mục nào bị cho là sai luật, không hợp pháp hoặc không có hiệu lực vì bất kỳ lí do luật pháp nào của bất kì tiểu bang hay đất nước nào mà những điều khoản này được định sẽ có hiệu lực, thì trong phạm vi cho phép của cơ quan có thẩm quyền, những điều khoản đó sẽ bị loại trừ và những điều khoản còn lại trong Quy định Sử dụng vẫn được sử dụng và giữ nguyên hiệu lực.</p>

                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_chinhsachbaomat" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#chinhsachbaomat_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1">Chính sách bảo mật</a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="chinhsachbaomat_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">

                                    <p><b>1. Mục đích:</b></p>

                                    <p>Hiện nay, vấn đề bảo mật thông tin đã trở thành vấn đề nóng trên tất cả các diễn đàn, điều này không chỉ gây khó khăn cho các tổ chức, cá nhân tham gia truy cập và giao dịch mà còn gây nên những phiền toái vượt ra ngoài tầm kiểm soát của hệ thống.</p>

                                    <p>Chính sách bảo mật và chia sẻ thông tin khách hàng của <a href="https://hasaki.vn">Hasaki.vn</a> như một lời cam kết với mong muốn nâng cao chất lượng dịch vụ, bảo đảm an toàn thông tin của các cá nhân, tổ chức khi tham gia truy cập hoặc giao dịch trực tiếp trên website <a href="https://hasaki.vn">Hasaki.vn</a>. Chúng tôi hiểu bảo vệ và sử dụng hợp lí thông tin của bạn cũng chính là bảo vệ lòng tin và sự yêu mến của bạn dành cho chúng tôi.</p>



                                    <p><b>2. Các điều khoản áp dụng</b></p>
                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;2.1. Cập nhật thông tin khách hàng:</p>

                                    <p>-                      Khi tham gia mở tài khoản hoặc giao dịch trực tiếp trên <a href="https://hasaki.vn">Hasaki.vn</a>, khách hàng cần phải cung cấp đầy đủ các thông tin bắt buộc bao gồm tên, địa chỉ, số điện thoại... cụ thể để có thể đáp ứng các điều kiện giao dịch cần thiết.</p>

                                    <p>-                      Các thông tin cá nhân quý khách cung cấp cho <a href="https://hasaki.vn">Hasaki.vn</a> cần đảm bảo độ chính xác và đầy đủ để phòng tránh trường hợp nhầm lẫn hoặc giả mạo. Mọi sự sai sót hoặc không chính xác trong thông tin được cung cấp sẽ ảnh hưởng trực tiếp đến quyền lợi của quý khách hàng. Do đó, <a href="https://hasaki.vn">Hasaki.vn</a> sẽ không chịu trách nhiệm đối với những tranh chấp phát sinh có liên quan trong trường hợp này.</p>



                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;2.2.Lưu giữ và bảo mật thông tin khách hàng</p>

                                    <p>- Tất cả các thông tin khách hàng cung cấp và nội dung giao dịch đều được <a href="https://hasaki.vn">Hasaki.vn</a> lưu giữ bảo mật tuyệt đối trên hệ thống. <a href="https://hasaki.vn">Hasaki.vn</a> cam đoan sẽ không bán hoặc chia sẻ dẫn đến làm lộ thông tin cá nhân của bạn, không vì những mục đích thương mại mà vi phạm cam kết của chúng tôi ghi trong chính sách bảo mật này</p>

                                    <p>- <a href="https://hasaki.vn">Hasaki.vn</a> luôn sẵn sàng về đội ngũ kĩ thuật và an ninh để có những biện pháp đối phó với những trường hợp cố tình xâm nhập và sử dụng trái phép thông tin của khách hàng. Khi thu thập dữ liệu, <a href="https://hasaki.vn">Hasaki.vn</a> thực hiện lưu giữ và bảo mật thông tin khách hàng tại hệ thống máy chủ và các thông tin khách hàng này được bảo đảm an toàn bằng các hệ thống bảo vệ tốt nhất hiện nay, cùng các biện pháp kiểm soát truy cập và mã hóa dữ liệu.</p>

                                    <p>- Khách hàng không được phép sử dụng bất kì chương trình hay công cụ nào nhằm mục đích khai thác, thay đổi dữ liệu bất hợp pháp trên hệ thống <a href="https://hasaki.vn">Hasaki.vn</a>. Mọi hành vi cố tình xâm phạm, tùy theo tính chất sự việc, chúng tôi có quyền khởi tố với các cơ quan có thẩm quyền theo quy định pháp luật hiện hành.</p>

                                    <p>- Khách hàng nên tự bảo vệ thông tin bảo mật của mình bằng cách không chia sẻ các thông tin cá nhân cũng như các thông tin giao dịch với bên thứ ba, cẩn thận trong việc đăng nhập/đăng xuất tài  khoản để loại trừ những sự cố rò rỉ thông tin không đáng có.</p>



                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;2.3.Sử dụng thông tin</p>

                                    <p><a href="https://hasaki.vn">Hasaki.vn</a> có quyền sử dụng hợp pháp các thông tin cá nhân của khách hàng trong các trường hợp để đảm bảo quyền lợi của quý khách như:</p>

                                    <p>-Thông báo các thông tin về dịch vụ quảng cáo, các chương trình khuyến mãi… đến khách hàng theo nhu cầu và thói quen của quý khách khi truy cập</p>

                                    <p>-Liên lạc và hỗ trợ khách hàng trong những trường hợp đặc biệt</p>

                                    <p>-Phát hiện và ngăn chặn ngay lập tức các hành vi can thiệp hoặc phá hoại tài khoản của khách hàng</p>



                                    <p>&nbsp;&nbsp;&nbsp;&nbsp;2.4.Chia sẻ thông tin</p>

                                    <p><a href="https://hasaki.vn">Hasaki.vn</a> cam kết không tiết lộ thông tin khách hàng cho bên thứ ba ngoại trừ các trường hợp sau:</p>

                                    <p>- Thực hiện theo yêu cầu của các các cá nhân, tổ chức có thẩm quyền theo quy định của pháp luật</p>

                                    <p>-Cần thiết phải sử dụng các thông tin để phục vụ cho việc cung cấp các dịch vụ/tiện ích cho khách hàng

                                    </p><p>-Nghiên cứu thị trường và đánh giá phân tích</p>

                                    <p>-  Trao đổi thông tin khách hàng với đối tác hoặc đại lí phân phối để phân tích nâng cao chất lượng dịch vụ</p>

                                    <p>- Ngoài các trường hợp trên, khi cần phải trao đổi thông tin khách hàng cho bên thứ ba, <a href="https://hasaki.vn">Hasaki.vn</a> sẽ thông báo trực tiếp với khách hàng và sẽ chỉ thực hiện việc trao đổi thông tin khi có sự đồng thuận từ phía khách hàng.</p>



                                    <p><b>3. Liên hệ và giải đáp thắc mắc</b></p>
                                    <p>
                                        Mọi ý kiến đóng góp/nhu cầu hỗ trợ vui lòng gọi vào Hotline: <b>1900 636 900 </b>, Email: <a href="email" class="txt_color_1">hotro@hasaki.vn</a> hoặc liên hệ trực tiếp tại địa chỉ: Công ty <b>TNHH HASAKI BEAUTY &amp; SPA</b><br>
                                        - CN 1: 71 Hoàng Hoa Thám, Phường 13, Quận Tân Bình, TP. HCM<br>
                                        - CN 2: 555 đường 3 tháng 2, phường 8, quận 10<br>
                                        -CN 3: 176 Phan Đăng Lưu, Phường 3, Quận Phú Nhuận<br>
                                        -CN 4: 94 Lê Văn Việt, Phường Hiệp Phú, Quận 9
                                    </p>

                                </div>
                            </div>

                        </div>
                        <div class="content_tab_left tab-pane" id="tab_hasakigo" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#hasakigo_fck_1" aria-expanded="true" aria-controls="hasakigo_fck_1">HasakiGo là gì? </a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="hasakigo_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <p>Là dịch vụ giao hàng 2H của Hasaki nhằm đáp ứng nhu cầu nhận hàng nhanh trong ngày dành cho các khách hàng thuộc khu vực nội thành TP. Hồ Chí Minh.</p>
                                    <p><strong>Đối tượng nào sử dụng dịch vụ HasakiGo?</strong></p>
                                    <p>Khách hàng nội thành TP Hồ Chí Minh xem chi tiết <a href="https://hasaki.vn/giao-hang-duoi-120-phut.html" target="_blank" class="txt_color_1">tại đây</a> </p>
                                    <p><strong>Mức phí áp dụng cho dịch vụ giao hàng 2H?</strong></p>
                                    <!--                                <p>Tin vui cho các bà mẹ, những đơn hàng có chứa sản phẩm Ba Mẹ và Bé - Đồ chơi trên 90k sẽ được miễn phí vận chuyển cho dịch vụ giao hàng 2H. Cụ thể cước phí sẽ được áp dụng như sau.</p>-->
                                    <div class="space_bottom_15"><strong>Cước phí:</strong><br>
                                        <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                                            <tbody>
                                            <tr>
                                                <td><strong>Điều kiện đơn hàng</strong></td>
                                                <td><strong>Mức phí</strong></td>
                                            </tr>
                                            <tr>
                                                <td>Đơn hàng từ 90.000đ</td>
                                                <td>9.000đ</td>
                                            </tr>
                                            <tr>
                                                <td>Đơn hàng nhỏ hơn 90.000đ</td>
                                                <td>19.000đ</td>
                                            </tr>
                                            <!-- <tr>
                                                <td>Đơn hàng có tổng giá trị sản phẩm Bé - Đồ Chơi từ 90.000đ</td>
                                                <td>0đ (trợ giá 19.000đ)</td>
                                            </tr>
                                            <tr>
                                                <td>Đơn hàng có tổng giá trị sản phẩm Bé - Đồ Chơi nhỏ hơn 90.000đ</td>
                                                <td>10.000đ (trợ giá 19.000đ)</td>
                                            </tr>
                                            <tr>
                                                <td>Đơn hàng khác từ 90.000đ</td>
                                                <td>19.000đ</td>
                                            </tr>
                                            <tr>
                                                <td>Đơn hàng khác nhỏ hơn 90.000đ</td>
                                                <td>29.000đ</td>
                                            </tr> -->
                                            </tbody></table>
                                    </div>
                                    <p><strong>Khung giờ nào áp dụng dịch vụ giao hàng 2H?</strong></p>
                                    <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                                        <tbody><tr>
                                            <td><strong>Thời gian đặt hàng</strong></td>
                                            <td><strong>Dự kiến nhận hàng</strong></td>
                                        </tr>
                                        <tr>
                                            <td>Từ 00:01 - 08:00</td>
                                            <td>Trước 10:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 08:01 - 09:00</td>
                                            <td>Trước 11:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 09:01 - 10:00</td>
                                            <td>Trước 12:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 10:01 - 11:00</td>
                                            <td>Trước 13:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 11:01 - 12:00</td>
                                            <td>Trước 14:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 12:01 - 13:00</td>
                                            <td>Trước 15:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 13:01 - 14:00</td>
                                            <td>Trước 16:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 14:01 - 15:00</td>
                                            <td>Trước 17:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 15:01 - 16:00</td>
                                            <td>Trước 18:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 16:01 - 18:00</td>
                                            <td>Trước 20:00 cùng ngày</td>
                                        </tr>
                                        <tr>
                                            <td>Từ 18:01 - 00:00</td>
                                            <td>Trước 10:00 ngày kế tiếp</td>
                                        </tr>
                                        </tbody></table>

                                    <p><strong>Khu vực nào áp dụng dịch vụ?</strong></p>
                                    <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                                        <tbody>


                                        <tr id="row-district-id-1">
                                            <td>Quận 1</td>
                                            <td id="cell-ward-id-">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>





                                        <tr id="row-district-id-3">
                                            <td>Quận 3</td>
                                            <td id="cell-ward-id-">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-4">
                                            <td>Quận 4</td>
                                            <td id="cell-ward-id-">
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 15</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 3</div>
                                                <div class="item_line">Phường 4</div>
                                                <div class="item_line">Phường 14</div>
                                                <div class="item_line">Phường 5</div>
                                                <div class="item_line">Phường 10</div>
                                                <div class="item_line">Phường 8</div>
                                                <div class="item_line">Phường 6</div>
                                                <div class="item_line">Phường 9</div>

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-5">
                                            <td>Quận 5</td>
                                            <td id="cell-ward-id-27256">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-6">
                                            <td>Quận 6</td>
                                            <td id="cell-ward-id-27256">
                                                <div class="item_line">Phường 3</div>
                                                <div class="item_line">Phường 8</div>
                                                <div class="item_line">Phường 4</div>
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 5</div>
                                                <div class="item_line">Phường 12</div>
                                                <div class="item_line">Phường 6</div>
                                                <div class="item_line">Phường 9</div>
                                                <div class="item_line">Phường 14</div>

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-7">
                                            <td>Quận 7</td>
                                            <td id="cell-ward-id-27346">
                                                <div class="item_line">Phường Tân Phong</div>
                                                <div class="item_line">Phường Tân Phú</div>
                                                <div class="item_line">Phường Bình Thuận</div>
                                                <div class="item_line">Phường Tân Hưng</div>
                                                <div class="item_line">Phường Tân Kiểng</div>
                                                <div class="item_line">Phường Tân Thuận Tây</div>

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-8">
                                            <td>Quận 8</td>
                                            <td id="cell-ward-id-27466">
                                                <div class="item_line">Phường 14</div>
                                                <div class="item_line">Phường 12</div>
                                                <div class="item_line">Phường 13</div>
                                                <div class="item_line">Phường 10</div>
                                                <div class="item_line">Phường 9</div>
                                                <div class="item_line">Phường 11</div>
                                                <div class="item_line">Phường 3</div>
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 8</div>

                                            </td>
                                        </tr>





                                        <tr id="row-district-id-10">
                                            <td>Quận 10</td>
                                            <td id="cell-ward-id-27388">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-11">
                                            <td>Quận 11</td>
                                            <td id="cell-ward-id-27388">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>





                                        <tr id="row-district-id-13">
                                            <td>Quận Tân Bình</td>
                                            <td id="cell-ward-id-27388">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-14">
                                            <td>Quận Tân Phú</td>
                                            <td id="cell-ward-id-27388">
                                                <div class="item_line">Phường Tân Thới Hòa</div>
                                                <div class="item_line">Phường Hiệp Tân</div>
                                                <div class="item_line">Phường Hòa Thạnh</div>
                                                <div class="item_line">Phường Phú Trung</div>
                                                <div class="item_line">Phường Phú Thạnh</div>
                                                <div class="item_line">Phường Phú Thọ Hòa</div>
                                                <div class="item_line">Phường Tân Thành</div>
                                                <div class="item_line">Phường Tân Quý</div>
                                                <div class="item_line">Phường Tây Thạnh</div>
                                                <div class="item_line">Phường Tân Sơn Nhì</div>

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-15">
                                            <td>Quận Phú Nhuận</td>
                                            <td id="cell-ward-id-27010">
                                                <div class="item_line">Phường 13</div>
                                                <div class="item_line">Phường 12</div>
                                                <div class="item_line">Phường 14</div>
                                                <div class="item_line">Phường 17</div>
                                                <div class="item_line">Phường 11</div>
                                                <div class="item_line">Phường 10</div>
                                                <div class="item_line">Phường 8</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 3</div>
                                                <div class="item_line">Phường 7</div>
                                                <div class="item_line">Phường 9</div>
                                                <div class="item_line">Phường 5</div>
                                                <div class="item_line">Phường 4</div>

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-16">
                                            <td>Quận Gò Vấp</td>
                                            <td id="cell-ward-id-27043">
                                                Toàn bộ khu vực

                                            </td>
                                        </tr>



                                        <tr id="row-district-id-17">
                                            <td>Quận Bình Thạnh</td>
                                            <td id="cell-ward-id-27043">
                                                <div class="item_line">Phường 24</div>
                                                <div class="item_line">Phường 11</div>
                                                <div class="item_line">Phường 26</div>
                                                <div class="item_line">Phường 12</div>
                                                <div class="item_line">Phường 5</div>
                                                <div class="item_line">Phường 7</div>
                                                <div class="item_line">Phường 6</div>
                                                <div class="item_line">Phường 14</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 3</div>

                                            </td>
                                        </tr>

















                                        <!-- <tr>
                                            <td>Quận Tân Phú</td>
                                            <td>Toàn bộ khu vực</td>
                                            <td>
                                                <div class="item_line">Phường Phú Thạnh</div>
                                                <div class="item_line">Phường Hiệp Tân</div>
                                                <div class="item_line">Phường Phú Trung</div>
                                                <div class="item_line">Phường Hòa Thạnh</div>
                                                <div class="item_line">Phường Sơn Kỳ</div>
                                                <div class="item_line">Phường Tân Thới Hòa</div>
                                            </td>
                                        </tr> -->
                                        <!-- <tr>
                                            <td>Quận Bình Thạnh</td>
                                            <td>
                                                <div class="item_line">Phường 1</div>
                                                <div class="item_line">Phường 2</div>
                                                <div class="item_line">Phường 3</div>
                                                <div class="item_line">Phường 5</div>
                                                <div class="item_line">Phường 6</div>
                                                <div class="item_line">Phường 7</div>
                                                <div class="item_line">Phường 11</div>
                                                <div class="item_line">Phường 12</div>
                                                <div class="item_line">Phường 14</div>
                                                <div class="item_line">Phường 15</div>
                                                <div class="item_line">Phường 16</div>
                                                <div class="item_line">Phường 17</div>
                                                <div class="item_line">Phường 19</div>
                                                <div class="item_line">Phường 21</div>
                                                <div class="item_line">Phường 24</div>
                                                <div class="item_line">Phường 25</div>
                                                <div class="item_line">Phường 26</div>
                                            </td>
                                        </tr> -->
                                        </tbody></table>
                                    <p>Hasaki giao hàng đến 18h từ thứ 2 đến thứ 7.</p>

                                    <!--                                <p><strong>Khi nào đơn hàng được free ship 2H?</strong></p>-->
                                    <!--                                <p>Là các sản phẩm có gắn icon <img src="https://media.hasaki.vn/wysiwyg/Cover_categories/deliverynow.png" alt="" height="20px" class="icon_content_20"> trên web hasaki.vn hoặc các đơn hàng trên 90k có sản phẩm thuộc ngành hàng Ba mẹ và Bé - Đồ chơi. </p>-->


                                    <p><strong>Sản phẩm nào được áp dụng giao hàng 2H?</strong></p>
                                    <p>Tất cả các sản phẩm đăng bán trên website hasaki.vn đều được áp dụng dịch vụ giao hàng 2H.</p>

                                    <!--                                <p><strong>Làm sao để biết được sản phẩm nào thuộc ngành hàng Ba Mẹ và Bé - Đồ chơi?</strong></p>-->
                                    <!--                                <p>Tại trang chi tiết sản phẩm nếu quý khách thấy tên danh mục “Ba Mẹ và Bé - Đồ chơi” trên đường dẫn thì đó là sản phẩm thuộc ngành hàng Ba Mẹ và Bé - Đồ chơi.-->
                                    <!--                                </p>-->
                                    <!--                                <div class="text-center space_bottom_20"><img src="https://media.hasaki.vn/wysiwyg/Cover_categories/ba_me_va_be.png"></div>-->
                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_dichvuspa" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck ">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_1" aria-expanded="false" aria-controls="dichvuspa_fck_1" class="collapsed">Giờ làm việc của Hasaki Clinic &amp; Spa?</a>
                                </div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Hasaki Clinic &amp; Spa bắt đầu mở cửa phục vụ khách hàng từ 9h00 sáng và đóng cửa vào 8h00 tối mỗi ngày (kể cả thứ 7 &amp; Chủ Nhật).</p>
                                    <p>Sau 19h00 tối, Hasaki Clinic &amp; Spa sẽ không nhận thêm khách. Riêng các dịch vụ Massage Body chỉ nhận khách đến 18h30 tối.</p>
                                    <p>Để không mất nhiều thời gian chờ đợi, khách hàng nên đặt lịch hẹn trước qua website hoặc gọi đến số Hotline 1900 636 900 - nhấn phím 2 để được hỗ trợ.</p>
                                    <p>Thời gian nhận đặt lịch hẹn của khách từ 9h00 sáng đến 17h30 tối. Riêng thứ 7 &amp; Chủ Nhật không nhận đặt hẹn trước.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck ">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_1" aria-expanded="false" aria-controls="dichvuspa_fck_1" class="collapsed">Có cần đặt lịch trước khi đến spa Hasaki không?</a>
                                </div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_1" role="tabpanel" aria-labelledby="">
                                    <p>Để quá trình phục vụ được tốt nhất, Hasaki Clinic &amp; Spa khuyến nghị các khách hàng nên đặt lịch trước qua hotline hoặc inbox fanpage. Vào cuối tuần, thứ 7 và chủ nhật bên spa sẽ không nhận đặt lịch, bạn hãy tranh thủ ghé sớm để không phải chờ lâu nhé!</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_2" aria-expanded="false" aria-controls="dichvuspa_fck_2" class="collapsed">Đặt mua dịch vụ như thế nào?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_2" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Tương tự như các sản phẩm khác, bạn chỉ cần thêm dịch vụ vào giỏ hàng và tiến hành đặt hàng như bình thường. Shipper của Hasaki sẽ giao phiếu và nhận thanh toán tại địa chỉ trên đơn hàng.</p>
                                    <p>Với các dịch vụ chuyên sâu về da như nặn mụn, peel da, trị nám,... Hasaki khuyến nghị bạn nên đến trực tiếp 1 trong 5 chi nhánh Hasaki để được bác sĩ da liễu thăm khám da miễn phí và đưa ra dịch vụ chính xác nhất cho vấn đề bạn đang gặp phải.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_3" aria-expanded="false" aria-controls="dichvuspa_fck_3" class="collapsed">Khám da tại Hasaki có tốn phí không?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_3" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Khám da tại Hasaki Clinic &amp; Spa hoàn toàn miễn phí. Sau khi khám da, bác sĩ da liễu sẽ đề nghị các dịch vụ hoặc các sản phẩm điều trị bôi thoa, thuốc uống thích hợp cho làn da của bạn. Tuy nhiên quyết định chọn mua hoàn toàn là ở bạn, sẽ không có trường hợp bắt ép mua liệu trình hay các sản phẩm điều trị tại Hasaki.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_4" aria-expanded="false" aria-controls="dichvuspa_fck_4" class="collapsed">Những chi nhánh nào của Hasaki có bác sĩ da liễu?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_4" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Hiện tại 5 chi nhánh của Hasaki đều đã có các dịch vụ phòng khám và bác sĩ da liễu trực theo ngày. Bạn có thể gọi đến hotline chi nhánh Hasaki Clinic &amp; Spa gần nhất để đặt lịch khám bác sĩ nhanh nhất nhé!</p>
                                    <p>
                                        CN1: 71 Hoàng Hoa Thám, Phường 1, Quận Tân Bình - 0911.483.663<br>
                                        CN2: 555 Đường 3 Tháng 2, Phường 8, Quận 10 - 0948.015.335<br>
                                        CN3: 176 Phan Đăng Lưu, Phường 3, Quận Phú Nhuận - 0917.361.582<br>
                                        CN4: 94 Lê Văn Việt, Phường Hiệp Phú, Q.9 - 0825.026.004<br>
                                        CN5: 119 Nguyễn Gia Trí, Phường 25, Quận Bình Thạnh - 0837.994.119<br>
                                        Hotline: 1900 636 900 - nhấn phím 2 để được hỗ trợ.
                                    </p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_5" aria-expanded="false" aria-controls="dichvuspa_fck_5" class="collapsed">Hasaki Clinic &amp; Spa có chương trình gì hằng tháng?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_5" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Để cập nhật nhanh nhất những chương trình của spa và phòng khám hằng tháng, bạn hãy theo dõi fanpage Hasaki Clinic &amp; Spa nhé! (link: https://www.facebook.com/hasakispa/)</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_6" aria-expanded="false" aria-controls="dichvuspa_fck_6" class="collapsed">Các dịch vụ thư giãn có dành cho nam không?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_6" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Hiện tại Hasaki chỉ mới cung cấp các dịch vụ spa, thư giãn cho khách hàng nữ. Đối với những dịch vụ phòng khám, điều trị chuyên sâu về da thì cả khách hàng nam và nữ đều có thể sử dụng.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_7" aria-expanded="false" aria-controls="dichvuspa_fck_7" class="collapsed">Mua dịch vụ rồi có hoàn trả được hay không?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_7" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Hasaki Clinic &amp; Spa không áp dụng hoàn trả tiền mặt trong bất cứ trường hợp nào. Bạn có thể chuyển qua dùng dịch vụ tương đương hoặc bù thêm chi phí để sử dụng các dịch vụ khác phù hợp hơn.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_8" aria-expanded="false" aria-controls="dichvuspa_fck_8" class="collapsed">Tôi cần tư vấn về dịch vụ spa và phòng khám</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_8" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Bạn có thể liên hệ trực tiếp hotline 1900 636 900 - nhấn phím 2 hoặc để lại thông tin trên fanpage Hasaki Clinic &amp; Spa (link: https://www.facebook.com/hasakispa/) để được giải đáp thắc mắc nhanh nhất.</p>
                                </div>
                            </div>
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#dichvuspa_fck_9" aria-expanded="false" aria-controls="dichvuspa_fck_9" class="collapsed">Tôi có thể sang nhượng dịch vụ cho người khác không?</a></div>
                                <div class="fck_support_page panel-collapse collapse" id="dichvuspa_fck_9" role="tabpanel" aria-labelledby="" aria-expanded="false">
                                    <p>Nếu không còn nhu cầu sử dụng bạn vẫn có thể chuyển cho các tài khoản spa tại Hasaki khác, tuy nhiên không áp dụng hoàn trả tiền và không áp dụng cho các dịch vụ được tặng thêm.</p>
                                </div>
                            </div>
                        </div>


                        <div class="content_tab_left tab-pane" id="tab_khachhangthanthiet" role="tabpanel">
                            <div class="item_support_page">
                                <div class="fck_support_page panel-collapse collapse in" id="hasakigo_fck_1" role="tabpanel" aria-labelledby="">
                                    <div class="space_bottom_10"><img id="lamp" title="Tri ân khách hàng" src="https://media.hasaki.vn/wysiwyg/tichdiem_1320_v2.jpg" alt="Tri ân khách hàng"></div>
                                    <div class="block_text_trian relative space_bottom_20">
                                        <div class="space_bottom_10"><strong>Đối tượng áp dụng:</strong></div>
                                        <div class="text_intro_2">
                                            Tất cả các khách hàng đã mua sắm tại <a href="https://hasaki.vn/">Hasaki.vn</a> có tổng hóa đơn mua sắm tích đủ <strong class="txt_18">1000 điểm</strong> sẽ được tính tương đương với một lần khuyến mãi 5%. (Điểm được cộng dồn trên tất cả các đơn hàng cả online và offline). <br>Ngoài ra, khi tích đủ <strong class="txt_18">5000 điểm</strong> quý khách sẽ được nhận thêm 01 ưu đãi dịch vụ.
                                        </div>
                                    </div>
                                    <div class="block_text_trian relative space_bottom_20">
                                        <div class="space_bottom_10"><strong>Nội dung khuyến mãi:</strong></div>
                                        <div class="text_intro_2">
                                            <!--                                        <p>Khi tích đủ điểm, Quý khách sẽ nhận được một tin nhắn thông báo về chương trình khuyến mãi và một mã voucher khuyến mãi.</p>-->
                                            <p>Khách hàng thân thiết sẽ nhận được các ưu đãi như sau:</p>
                                            <p>Khi tổng tích điểm đạt mốc <strong>1000 điểm</strong>:</p>
                                            <p>- Quý khách sẽ được 01 lần giảm 5% khi đặt mua mỹ phẩm online tại trang Hasaki.vn hoặc mua trực tiếp tại cửa hàng. Nếu đặt hàng online chỉ cần nhập mã voucher, hệ thống sẽ tự động giảm 5% giá trị đơn hàng cho quý khách. Nếu đến trực tiếp tại Shop, khi thanh toán vui lòng yêu cầu thu ngân để được giảm giá.</p>
                                            <p>Khi tổng tích điểm đạt mốc <strong>5000 điểm</strong>:</p>
                                            <p>Quý khách hàng sẽ được tặng thêm 1 lần trải nghiệm liệu trình <strong>Chăm Sóc Da Mặt Cơ Bản</strong> trị giá <strong>250.000đ</strong>.</p>
                                            <p><strong>Lưu ý:</strong> </p>
                                            <p>Để được sử dụng mã Voucher ưu đãi khi đặt mua mỹ phẩm trực tuyến tại <strong><a href="https://hasaki.vn/" class="txt_color_1">Hasaki.vn:</a></strong></p>
                                            <p>- Khách hàng chưa có tài khoản mua sắm trực tuyến tại <strong><a href="https://hasaki.vn/" class="txt_color_1">Hasaki.vn</a></strong>, vui lòng <b>tạo tài khoản và cung cấp chính xác số điện thoại</b> cũng như các thông tin cá nhân, trước khi tiến hành đặt mua.</p>
                                            <p>- Khách hàng đã có tài khoản mua sắm trực tuyến tại <strong><a href="https://hasaki.vn/" class="txt_color_1">Hasaki.vn</a></strong>, vui lòng <b>đăng nhập tài khoản</b> trước khi mua sắm</p>

                                            <p>Mỗi mã voucher chỉ áp dụng 1 lần duy nhất. . </p>
                                            <p>Đối với dịch vụ tại Spa, trước khi đến quý khách vui lòng liên hệ trước cũng như cung cấp mã voucher để nhân viên có sắp xếp lịch hẹn. </p>
                                        </div>
                                    </div>
                                    <div class="block_text_trian relative space_bottom_20">
                                        <div class="space_bottom_10"><strong>Hạn sử dụng mã Voucher:</strong></div>
                                        <div class="text_intro_2">
                                            <p>45 ngày kể từ ngày ra thông báo</p>
                                        </div>
                                    </div>
                                    <div class="block_text_trian relative space_bottom_20">
                                        <div class="space_bottom_10"><strong>Các trường hợp không được tích điểm và áp dụng mã khuyến mãi:</strong></div>
                                        <div class="text_intro_2">
                                            <p>Sản phẩm nằm trong chương trình Deals hoặc các chương trình khuyến mãi đặc biệt khác.</p>
                                            <p>Sản phẩm thuộc danh mục: Tã, Bỉm, Sữa</p>
                                            <p>Đơn hàng đã được áp dụng mã voucher giảm giá trên tổng bill.</p>
                                        </div>
                                    </div>

                                </div>


                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_mobile_gift" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#mobile_gift_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1"></a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="mobile_gift_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <div class="text-center txt_18 space_bottom_10 text-uppercase"><b>Thẻ quà tặng</b></div>
                                    <p>Hiện nay ngoài hình thức thanh toán bằng tiền mặt và thẻ tín dụng, trên thị trường đã và đang đang lưu hành một số hình thức thanh toán mới tiện lợi và thú vị hơn cho người tiêu dùng khi mua sắm. Để mang đến cho người dùng những trải nghiệm mới mẻ nhất khi mua sắm, hiện nay  <strong><a class="txt_color_1" href="https://hasaki.vn">Hasaki.vn</a></strong> đã tích hợp chương trình thẻ quà tặng <b></b> trên toàn hệ thống.</p>

                                    <p>Đây là giải pháp thanh toán mới thay thế cho phiếu quà tặng, phiếu mua hàng, phiếu mua sắm thông thường, giúp người tặng có thể trao quyền lựa chọn món quà theo đúng ý thích của người nhận.</p>
                                    <p>Hasaki tích hợp cả 2 loại thẻ hiện đang phát hành: Esteem Gift và Mobile Gift</p>
                                    <p>
                                        -   <b>Esteem Gift</b>: Thẻ chỉ sử dụng cho mua hàng trực tiếp tại các cửa hàng của Hasaki, quý khách vui lòng gửi thẻ quà tặng Esteem Gift cho nhân viên thu ngân trước khi thanh toán, giá trị thẻ tương đương với giá trị tiền mặt.
                                    </p>
                                    <p><strong>Cách thức sử dụng:</strong></p>
                                    <p>-    <b>Mobile Gift</b>: Với loại thẻ này, quý khách đồng thời có thể sử dụng cho cả đơn hàng online và offline của hệ thống của hàng Hasaki. </p>
                                    <p>+ <b>Với đơn hàng offline</b>: Quý khách vui lòng gửi mã thẻ cho nhân viên thu ngân trước khi thanh toán. Giá trị thẻ tương đương với giá trị tiền mặt.</p>
                                    <p>+ <b>Với đơn hàng online:</b> Tại màn hình checkout, quý khách vui lòng thực hiện các bước theo hướng dẫn sau:</p>
                                    <p>1.   Thực hiện các bước đặt hàng như các đơn hàng thông thường theo hướng dẫn <a href="https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang" class="txt_color_1"><b>https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang </b></a></p>
                                    <p>2.   Tại màn hình checkout, Quý khách vui lòng bấm chọn “<b>Chọn loại phiếu?</b>”</p>
                                    <div class="space_bottom_20"><img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/sodeso1.png" alt=""></div>
                                    <p>3.   Chọn loại phiếu Mobile Gift</p>
                                    <div class="space_bottom_20"><img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/sodeso2.png" alt=""></div>
                                    <p>4.   Nhập mã Mobile Gift và bấm Sử dụng</p>
                                    <div class="space_bottom_20"><img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/sodeso3.png" alt=""></div>
                                    <div class="space_bottom_20"><img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/sodeso4.png" alt=""></div>
                                    <p>
                                        <strong>Lưu ý:</strong> <br>
                                        o   Quý khách có thể sử dụng nhiều mã mobile gift bằng cách nhập và bấm sử dụng nhiều lần. <br>
                                        o   Quý khách cũng có thể sử dụng mã đã group của mobile gift
                                    </p>
                                    <p>5.   Kiểm tra lại thông tin, chọn hình thức vận chuyển và phương thức thanh toán sau đó bấm Đặt Hàng. Quý khách có thể kiểm tra lại thông tin đơn hàng bằng cách click vào mã đơn hàng.</p>
                                    <div class="space_bottom_20"><img title="" src="https://media.hasaki.vn/wysiwyg/supportCenter/sodeso5.png" alt=""></div>
                                    <p><b>Quy định sử dụng:</b></p>
                                    <p>
                                        Được thanh toán đúng với giá trị tương ứng được ghi trên thẻ. Không giới hạn số lượng thẻ trên cùng hóa đơn thanh toán
                                        Có thể kết hợp cùng các phương thức thanh toán khác cho mặt hàng có giá trị cao hơn giá trị của thẻ như: tiền mặt, thẻ tín dụng…</p>
                                    <p>Không có giá trị quy đổi ra thành tiền mặt, không hoàn lại tiền thừa.</p>
                                    <p>Không xuất hóa đơn tài chính cho giá trị của Thẻ khi mua hàng.</p>
                                    <p>Không hoàn tiền cho đơn hàng đổi trả sản phẩm.</p>
                                    <p style="font-weight: bold">Khách hàng dùng Mobile Gift khi mua hàng trực tuyến, khách không thể huỷ hoặc thay đổi đơn hàng sau khi xác nhận, nếu đơn hàng đó bị hủy do lỗi của Hasaki thì khách hàng sẽ được hoàn lại PHIẾU MUA HÀNG HASAKI có giá trị và thời hạn tương đương với Mobile Gift đã hủy trước đó</p>
                                    <p><b>Thẻ Esteem Gift hợp lệ.</b></p>
                                    <p>Thẻ có phần mã vạch nguyên vẹn, không bị bôi xóa hay chắp vá trên bề mặt.</p>
                                    <p>Thẻ còn hạn sử dụng. </p>
                                    <p>Không có chữ ký ở khu vực xác nhận sử dụng.</p>
                                    <p>*Mọi ý kiến thắc hoặc các trường hợp cần hỗ trợ quý khách vui lòng liên hệ Hotline: <strong class="txt_color_2">1900 636 900</strong> để được tư vấn cụ thể.</p>
                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_buyer_card" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#buyer_card_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1"></a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="buyer_card_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <div class="text-center txt_18 space_bottom_10 text-uppercase"><b>Phiếu Mua Hàng Và Phiếu Mua Dịch Vụ Hasaki</b></div>
                                    <p>Xuất phát từ mong muốn mang đến cho khách hàng những trải nghiệm mua sắm tiện lợi và hữu ích nhất, Hasaki luôn nỗ lực phát triển và hoàn thiện hơn dịch vụ của mình.</p>

                                    <p>Hasaki chính thức phát hành : <strong>Phiếu mua hàng Hasaki</strong> &amp; <strong>Phiếu Dịch Vụ Hasaki</strong> (áp dụng cho Spa) cũng không nằm ngoài mục tiêu đó. Sự ra đời của 2 loại phiếu này hứa hẹn sẽ giúp khách hàng rút ngắn được quy trình mua hàng/dịch vụ, các giao dịch thanh toán cũng sẽ trở nên dễ dàng và nhanh chóng hơn bao giờ hết.</p>
                                    <p>“Mua hàng không cần quẹt thẻ, cũng chẳng cần phải mang theo tiền. </p>
                                    <p>Tại sao không nhỉ?”</p>

                                    <p><strong>Giá trị của Phiếu mua hàng Hasaki:</strong></p>
                                    <p>Giá trị của Phiếu mua hàng và Phiếu Mua Dịch vụ Hasaki: <strong>100.000đ</strong>, <strong>200.000đ</strong> và <strong>500.000đ.</strong> Và 3 mệnh giá Phiếu Dịch Vụ với các giá trị <strong>100.000đ</strong>, <strong>500.000đ</strong>. và <strong>1.000.000đ</strong>.</p>

                                    <p>Quý khách có thể đặt mua phiếu trực tiếp tại cửa hàng hoặc mua online tại link <a href="https://hasaki.vn/phieu-mua-hang.html" class="txt_color_1"><b>https://hasaki.vn/phieu-mua-hang.html</b></a>
                                    </p>

                                    <p><strong>Thời hạn sử dụng:</strong></p>
                                    <p>Thời hạn sử dụng của phiếu mua hàng Hasaki là 1 năm kể từ ngày phát hành. Với khung thời gian này quý khách có thể thoải mái sử dụng mà không cần canh cánh nổi lo về thời hạn của phiếu.</p>
                                    <p><strong><i>Lưu ý: Phiếu Dịch Vụ Hasaki chỉ sử dụng trực tiếp tại các chi nhánh SPA Hasaki, không áp dụng khi mua dịch vụ online.</i></strong></p>
                                    <p><strong>Hướng dẫn sử dụng:</strong></p>
                                    <p><strong>*Trường hợp mua hàng Offline (mua hàng trực tiếp tại cửa hàng)</strong></p>
                                    <p>Trước khi thanh toán, Quý khách vui lòng đưa phiếu mua hàng Hasaki cho nhân viên thu ngân để nhân viên thu ngân có thể phục vụ bạn tốt nhất.</p>
                                    <p><strong>*Trường hợp mua hàng Online</strong></p>
                                    <p>1.   Thực hiện các bược đặt hàng như các đơn hàng thông thường theo hướng dẫn <a href="https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang" class="txt_color_1"><b>https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang</b></a></p>
                                    <p>2.   Tại màn hình checkout, Quý khách vui lòng bấm chọn “<b>Chọn loại phiếu?</b>”, chọn Hasaki</p>
                                    <div class="space_bottom_20"><img title="" src="/images/graphics/hsk_voucher_1.png" alt=""></div>

                                    <p>3.   Nhập mã Hasaki và bấm Sử dụng</p>
                                    <div class="space_bottom_20"><img title="" src="/images/graphics/hsk_voucher_2.png" alt=""></div>
                                    <div class="space_bottom_20"><img title="" src="/images/graphics/hsk_voucher_3.png" alt=""></div>

                                    <p><strong>Lưu ý:</strong>  Quý khách có thể sử dụng nhiều mã Hasaki bằng cách nhập và bấm sử dụng nhiều lần. </p>
                                    <p>4.   Bấm <strong>ĐẶT HÀNG</strong> để hoàn tất đơn hàng.</p>
                                    <p><strong>Quy định sử dụng:</strong></p>
                                    <p>
                                        Không giới hạn số lượng phiếu mua hàng.<br>
                                        Không giới hạn số lượng phiếu mua hàng trên cùng hóa đơn thanh toán.<br>
                                        Quý khách sẽ được thanh toán đúng với giá trị tương ứng được ghi trên phiế<br>u
                                        Có thể kết hợp cùng các phương thức thanh toán khác cho mặt hàng có giá trị cao hơn giá trị của phiếu như: tiền mặt, thẻ tín dụng…<br>
                                        Không có giá trị quy đổi ra thành tiền mặt, không hoàn lại tiền thừa.<br>
                                        Không xuất hóa đơn tài chính cho giá trị của Phiếu mua hàng Hasaki khi mua hàng.<br>
                                        Không hoàn tiền cho các đơn hàng đổi trả sản phẩm.<br>
                                    </p>
                                    <p><strong>    Phiếu mua hàng Hasaki hợp lệ.</strong></p>
                                    <p>
                                        Thẻ có phần mã vạch nguyên vẹn, không bị bôi xóa hay chắp vá trên bề mặt.<br>
                                        Thẻ còn hạn sử dụng.<br>
                                    </p>

                                    <p>*Mọi ý kiến thắc hoặc các trường hợp cần hỗ trợ quý khách vui lòng liên hệ Hotline: <strong class="txt_color_2">1900 636 900</strong> để được tư vấn cụ thể.</p>
                                </div>
                            </div>

                        </div>

                        <div class="content_tab_left tab-pane" id="tab_nhanhang_store" role="tabpanel">
                            <div class="item_support_page">
                                <div class="title_fck "><a data-toggle="collapse" data-parent="#accordion" href="#nhanhang_store_fck_1" aria-expanded="true" aria-controls="phivanchuyen_fck_1"></a></div>
                                <div class="fck_support_page panel-collapse collapse in" id="nhanhang_store_fck_1" role="tabpanel" aria-labelledby="" aria-expanded="true">
                                    <div class="text-center txt_16 space_bottom_10"></div>
                                    <p>Hasaki xin thông báo ra mắt Dịch vụ nhận hàng trực tiếp tại các chi nhánh. Dịch vụ này hướng đến mục tiêu giúp quý khách hàng có thể tiết kiệm thời gian hơn khi mua sắm tại Hasaki. Thay vì phải dành nhiều thời gian quanh quẩn chọn sản phẩm tại cửa hàng, quý khách có thể click chọn sản phẩm tại nhà qua webiste sau đó chỉ cần đến nhận và thanh toán. </p>
                                    <p>Hy vọng dịch vụ mới sẽ giúp bạn có những trải nghiệm thú vị hơn, hữu ích hơn khi mua sắm cùng Hasaki.</p>
                                    <p><strong>Thông tin dịch vụ:</strong></p>
                                    <p>Quý khách lựa chọn sản phẩm cần mua trưc tiếp tại website hasaki.vn và đặt hàng chỉ với vài bước đơn giản. Lưu ý chọn địa chỉ chi nhánh đến nhận hàng thay vì chọn hình thức giao hàng như thông thường. Đơn hàng hoàn tất sẽ được đóng gói sẵn tại chi nhánh mà quý khách chọn, quý khách chỉ cần đến nhận và thanh toán tại quầy. Hình thức mua hàng này sẽ giúp quý khách tiết kiệm được thời gian hơn thay vì phải loay hoay tìm kiếm sản phẩm cần mua tại cửa hàng.</p>
                                    <p><strong>Lưu ý:</strong></p>
                                    <p>Các sản phẩm trong đơn hàng thỏa mãn điều kiện cùng có hàng tại một trong những chi nhánh.</p>
                                    <p><strong>Thời hạn của đơn hàng:</strong></p>
                                    <p>Đối với đơn hàng đặt <strong> trước 20:00 giờ</strong>, các chi nhánh sẽ nhận giữ đơn hàng (đã đóng gói) của quý khách <strong>đến 20:00 giờ</strong> cùng ngày. <strong>Sau 20:00 giờ</strong> nếu quý khách không đến nhận, đơn hàng sẽ bị hủy. Những đơn hàng đặt <strong>sau 20:00</strong> giờ sẽ giữ <strong>đến 20:00 giờ</strong> ngày hôm sau</p>
                                    <p>Hướng dẫn đặt hàng:</p>
                                    <p>Sau khi đã bỏ sản phẩm vào giỏ hàng và chọn “<strong>Tiến hành đặt hàng</strong>”, Tại Mục “<strong>Vận chuyển và Thanh Toán</strong>”, quý khách click chọn ô “<strong>Nhận hàng tại Chi nhánh Hasaki</strong>” và chọn chi nhánh muốn nhận hàng. Sau đó thực hiện các bước đặt hàng như bình thường như: nhập mã voucher giảm giá (nếu có), nhập mã phiếu mua hàng (nếu có)... và hoàn tất bằng nút “Đặt hàng”</p>
                                    <p><img title="" src="/images/graphics/nhanhang_chinhanh.png" alt=""></p>
                                    <p>*Mọi thắc mắc và ý kiến đóng góp, vui lòng liên hệ hotline: <strong class="txt_color_2">1900 636 900</strong></p>
                                    <p>*Quý khách có thể xem thêm về hướng dẫn đặt hàng tại: <a href="https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang" class="txt_color_1" alt="Hướng dẫn đặt hàng">https://hasaki.vn/cau-hoi-thuong-gap#tab_huongdandathang</a></p>

                                </div>
                            </div>

                        </div>

                    </div>

            </div>


        </div>
        <!-- detail category -->

    </div>
@endsection()
@section('js')
    <script>
        $('.nav-tabs li a').click(function(e){
            e.preventDefault();
            e.stopImmediatePropagation();
            $(this).tab('show');
        });
    </script>
@stop