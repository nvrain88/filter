<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Trademark;
use DB;
class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        view()->composer('*',function($view){

            $trademarkIndex    = Trademark::where('status',1)->orderBy('id','DESC')->get();
            $data = [
                'trademarkIndex'    =>$trademarkIndex,
            ];

            $view->with($data);
        });
    }
}
