      <div class="row popup_quickbuy">
          <div class="col-md-6">
              <img src="{{ asset('public/img/product/'.$product->image) }}" class="img-responsive">
          </div>
          <div class="col-md-6">
              <h2 class="title_detail_product">{{ $product->name }}</h2>
              <p class="stt_product">{{ $product->sttname}}</p>
              
                @if($product->price_compare != 0)
                <div class="ml-2">
	                <input class="button-arrow button-down" type="button" value='-'></input>
	                <input type="text" name="quantity" id="quantity" maxlength="12" value="1" title="" class="input-text quantity" />
	                <input class="button-arrow button-up" type="button" value='+'></input>
	                <span class="new_price"><span class="nt7_price_fix"></span>{{ format_curency($product->price) }} </span>
                    <span class="old_price">{{ format_curency($product->price_compare) }} </span>
	            </div>
	            @else
	            <div class="ml-2" style="margin-bottom: 25px">
	                <input class="button-arrow button-down" type="button" value='-'></input>
	                <input type="text" name="quantity" id="quantity" maxlength="12" value="1" title="" class="input-text quantity" />
	                <input class="button-arrow button-up" type="button" value='+'></input>
	                <span class="new_price" style="margin-top: 10px"><span class="nt7_price_fix"></span>{{ format_curency($product->price) }} </span>
	            </div>
                @endif
	            {{-- <input type="hidden" name="id" id value="{{$product->idproduct}}"> --}}
	            <button id="btn_add_cart" idproduct="{{$product->idproduct}}" base_url="{{ url('') }}" token="{{csrf_token()}}" class="btn_add_cart btn btn-primary btn-xl"><i class="fa fa-shopping-cart"></i> <span style="text-transform: uppercase;font-size: 18px">{{ trans('index.add_cart') }}</span></button>
                <div class="description">{{ $product->description }}</div>
          </div>
      </div>

  <script>
    $("#btn_add_cart").click(function(e){
        e.preventDefault()
        quantity = $('#quantity').val()
        idproduct  = $(this).attr('idproduct')
        base_url  = $(this).attr('base_url')
        token     = $(this).attr('token')
      $.ajax({
          url: base_url+'/addcart_ajax/'+idproduct,
          type:'GET',
          cache:false,
          data:{"_token":token, "idproduct":idproduct,"quantity":quantity  },
          success:function(data){
            $('#modal_main_add_to_cart').modal('hide')
            $('#modal_main_title').html("")
            $('#modal_main_content').html("<div class='bg-success alert_add_cart'>"+data+"</div>")
            $('#modal_main').modal('show')
            var cart_quantity = Number($('#cart_quantity').text())
            cart_quantity = cart_quantity + Number(quantity);
            $('#cart_quantity').text(cart_quantity);
            if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
                setTimeout(function(){
                    location.reload();
                }, 2000)  
            }
          }
      })
      return false
    })
    $('.button-up').click(function() {
        $qty = $(this).parent().find('.quantity');
        qty = parseInt($qty.val()) + 1;
        $qty.val(qty);
    });
    $('.button-down').click(function() {
        $qty = $(this).parent().find('.quantity');
        qty = parseInt($qty.val()) - 1;
        if (qty <= 0)
            qty = 1;
        $qty.val(qty);
    });
</script>