@extends('computer.home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('content')
<div>
    <!-- Slider Section -->
    @if (Request::is('/'))
    @include('computer.home.slide-menu-home')
    @endif
    <!-- Slider Section /- -->

    <!-- Latest Updates -->
    <!-- <div class="container hidden-xs">
        <div class="latest-update col-md-2 no-padding">
            <h3>Latest Updates</h3>
        </div>
        <div class="col-md-10 latest-post-list no-padding">
            <div class="marquee"> 
                @foreach($news_new as $item)
                <a href="{{ url('tin-tuc/'.$item->slug) }}">
                    <img src="{{ asset('public/img/news/'.$item->newimg) }}" alt="latest-post-marquee"> 
                    {{ $item->newsname }}
                </a>
                @endforeach  
            </div>
        </div>
    </div>  -->
    <!-- Latest Updates /- -->
    <!-- slide -->

    
    @if (Request::is('/'))
        <div class="visible-xs" style="margin-left: 5px; margin-right: 5px; margin-top: 85px;">
    
            <div id='coin-slider-home'>
                @foreach($slides as $row)
                  <a href="{{$row->linknew}}" target="_blank">
                    <img src='{{url('public/img/slide/'.$row->img)}}'>
                    
                  </a>
                @endforeach                  
            </div>
        </div>
    @endif
<!-- end slide -->
    @if (Request::is('/'))
        <div class="visible-xs" style="margin-left: 5px; margin-right: 5px;">
            <div class="row">                    
                    @foreach($adverts_bottom as $item)
                        <div class="col-md-4 index-ads">
                            <div class="nn-banner-qc-index">
                              <a href="{{ $item->link }}"><img src="{{ asset('public/img/advert/'.$item->img) }}"></a>
                              <!-- <p>Banner quảng cáo</p> -->
                            </div>
                        </div>
                    @endforeach                    
                </div>
        </div>
    @endif
    

    <!-- product -->
        <div id="new-products-section" class="section-padding new-products-section owl-carousel-section">
            <!-- Section Header -->
                <h2 style="text-align:center;">SẢN PHẨM BÁN CHẠY</h2>
            <!-- Section Header /- -->
            
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <div id="new-products-slider" class="owl-carousel owl-theme">

                    @foreach($new_products as $itemproduct) 
                        <div class="item">
                            <div class="col-md-12">
                            <div class="product-item">
                                  <div class="pi-img-wrapper">
                                    <img src="{{ asset('public/img/product/'.$itemproduct->image) }}" alt="Berry Lace Dress" width="300px" height="200px">
                                    <div>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                    </div>
                                  </div>
                                  <p class="product-name"><a href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a></p>
                                  <div class="product-price">
                                        @if($itemproduct->price != 0)
                                        <span>{{ format_curency($itemproduct->price) }}</span>
                                        @else
                                        <span style="text-transform: uppercase">{{ trans("index.contact") }}</span>
                                        @endif
                                    </div>
                                        @if($itemproduct->price != 0 && !empty($itemproduct->price))
                                            <button class="btn btn_add_cart" idproduct="{{ $itemproduct->idproduct }}" base_url="{{ url("") }}" token="{{ csrf_token() }}"><i class="fa fa-cart-plus fa-1x"><span class="hidden-xs"> MUA NGAY</span></i></button>
                                        @endif
                                  <div class="sticker sticker-new" style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">
                                      
                                  </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div><!-- container /- -->
        </div><!-- Political & World Over  -->    
    <!-- /PRODUCT -->
        <!--<div id="main-home" class="container">-->
        <!--    <div class="row list-book mb-3">-->
        <!--    <div class="title-list-book col-md-12 text-center">-->
        <!--        <h2>THƯƠNG HIỆU</h2>-->
        <!--    </div>-->
        <!--    <div class="col-md-12 slide-books slider">-->
        <!--        @foreach($trademarkIndex as $item)-->
        <!--            <div class="slide col-xs-12 col-sm-12 col-md-4 book-item">-->
        <!--                <a href="{{ route('home.trademark',$item->id) }}">-->
        <!--                    <img title="{{ $item->title }}" src="{{ asset('public/img/trademark/'.$item->image) }}" class="img-fluid">-->
        <!--                    <h5 title="{{ $item->title }}" style="font-size: 16px;text-transform: capitalize;">{{ $item->title }}</h5>-->
        <!--                </a>-->
        <!--            </div>-->
        <!--        @endforeach-->
        <!--    </div>-->
        <!--</div>-->
         <!-- Category product -->
         <div id="political-world-section" class="section-padding political-world-section owl-carousel-section">
            <!-- Section Header -->
                <h2 style="text-align:center;">THƯƠNG HIỆU</h2>
            <!-- Section Header /- -->
            
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <div id="political-world" class="owl-carousel owl-theme">

                        @foreach($trademarkIndex as $item)
                        <div class="item">
                            <div class="col-md-12">
                                <div class="post-box">
                                    <div class="image-box" style="background-image:url({{ asset('public/img/trademark/'.$item->image) }}); " ></div>
                                    <div class="box-content">
                                        <a href="{{ url('trademark/'.$item->id) }}" class="block-title">{{ $item->title }}</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                    </div>
                </div>
            </div><!-- container /- -->
        </div><!-- Political & World Over  -->
  
        <div class="container category_product"> 
            <div class="section-padding section-white slider-section">
                <h2 style="text-align:center;">DANH MỤC SẢN PHẨM</h2>
                <div class="col-md-12">
                    <div class="row">                    
                        <div class="col-md-4 index-cat-product">
                            <div class="nn-banner-qc-index">
                                <a href="https://shop.lavendercare.vn/loai-san-pham/thiet-ke-website">
                                    <img src="./public/img/advert/product-category.jpg">
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4 index-cat-product">
                            <div class="nn-banner-qc-index">
                              <a href="https://shop.lavendercare.vn/loai-san-pham/thiet-ke-website"><img src="./public/img/advert/product-category.jpg"></a>
                            </div>
                        </div>
                        <div class="col-md-4 index-cat-product">
                            <div class="nn-banner-qc-index">
                              <a href="https://shop.lavendercare.vn/loai-san-pham/thiet-ke-website"><img src="./public/img/advert/product-category.jpg"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Political & World Over  -->
        <div id="political-world-section" class="section-padding political-world-section owl-carousel-section">
            <!-- Section Header -->
                <h2 style="text-align:center;">TIN MỚI NHẤT</h2>
            <!-- Section Header /- -->
            
            <!-- Container -->
            <div class="container">
                <div class="row">
                    <div id="political-world" class="owl-carousel owl-theme">

                        @foreach($news_new as $item)
                        <div class="item">
                            <div class="col-md-12">
                                <div class="post-box">
                                    <div class="image-box" style="background-image:url({{ asset('public/img/news/'.$item->newimg) }}); " ></div> 
                                    <ul class="comments-social">
                                        <li><a href="{{ url('tin-tuc/'.$item->slug) }}" class="btn_share_fb btn_top_link" ><i class="fa fa-share-alt fa-1x"></i></a></li>
                                    </ul>
                                    <div class="post-box-inner">
                                        <a href="{{ url('tin-tuc/'.$item->slug) }}" class="box-read-more"><i class="fa fa-arrow-right"></i> Xem thêm</a>
                                        <div class="box-content">
                                            <a href="{{ url('tin-tuc/'.$item->slug) }}" class="block-title">{{ $item->newsname }}</a>
                                            <p class="time"><i class="fa fa-clock-o"></i> {{ $item->created_at }}</p>
                                            <p>{{ $item->newintro }}</p>
                                            <!-- <a href="#"><i class="fa fa-heart"></i> 8</a>
                                            <a href="#"><img src="images/icon/comment-icon.png" alt="comment" /> 13</a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach

                    </div>
                </div>
            </div><!-- container /- -->
        </div><!-- Political & World Over  -->    
        
        <script type="text/javascript">
              $(document).ready(function() {
                var w  = screen.width;
                $('#coin-slider-home').coinslider({ width: w-10 ,height: 200, navigation: true,showNavigationButtons: true, delay: 2000 });
                $('.slide-books').slick({
    		        slidesToShow: 4,
    		        slidesToScroll: 1,
    		        autoplay: true,
    		        autoplaySpeed: 1500,
    		        arrows: true,
    		        dots: false,
    		        pauseOnHover: true,
    		        prevArrow: '<span class="nav-arrow-prev fa fa-chevron-left " aria-hidden="true"></span>',
    	         	nextArrow: '<span class="nav-arrow-next fa fa-chevron-right" aria-hidden="true"></span>',
    		        responsive: [{
    		            breakpoint: 768,
    		            settings: {
    		                slidesToShow: 4
    		            }
    		        }, {
    		            breakpoint: 520,
    		            settings: {
    		                slidesToShow: 1
    		            }
    		        }]
    		    });
              });
        </script>
</div>
@endsection()
@section('css')
    <style>
#main-home .list-book .book-item {
    text-align: center;
    overflow: hidden;
}

#main-home .list-book .book-item a:hover{
    text-decoration: none;
}

#main-home .list-book .book-item img{
    width: 100%;
    height: 400px;
    margin-bottom: 15px;
}

#main-home .list-book .book-item h5{
    text-transform: uppercase;
    font-weight: 600;
    color: #4d89c7;
    margin-bottom: 0;
}

#main-home .list-book .book-item p{
    color: #777;
}
/*slide book*/
#main-home .list-book .slide-books .slick-arrow.nav-arrow-prev{
    position: absolute;
    top: 40%;
    left: -5px;
}

#main-home .list-book .slide-books .slick-arrow.nav-arrow-next{
    position: absolute;
    top: 40%;
    right: -10px;
}
/*slide book*/
    </style>
@stop