<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trademark extends Model
{
    protected $table = "trademarks";
   	public $timestamps = false;
   	
   	public function hotProducts() {
        return $this->hasMany(Product::class,'trademark_id','id');
    }
}
