<?php //session_start(); ?>
@extends('computer.home.master')
@section('title', (!empty($detail_cat)?$detail_cat->trname:""))
@section('seo_keyword', (!empty($detail_cat)?$detail_cat->trname:""))
@section('seo_description', (!empty($detail_cat)?$detail_cat->description:""))
@section('seo_image', (!empty($detail_cat->modimg))?asset($detail_cat->modimg): ((!empty($detail_cat->listimg))?asset($detail_cat->listimg):"" ) )
@section('seo_url', url()->current())
@section('content')


    <?php

    $fullLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
		    "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
	    $_SERVER['REQUEST_URI'];

    $bak = $fullLink;

    $_SESSION['old_cat1'] = $bak;


    ?>
<div class="wrapper_main container">
    <!-- quang cáo -->

    <!-- breadcrumb  --> 
      <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
            @if ( $check_cat_level == 2 )
                <li><i class="fa fa-chevron-right"></i><a href="{{ url('loai-san-pham/'.$product_cat->slug) }}">{{ $product_cat->modname }}</a></li>
            @endif
                <li><i class="fa fa-chevron-right"></i><span>{{ $detail_cat->trname }}</span></li>
        </ul>
      </div> 
    <!-- breadcrumb  -->



    <!-- detail category --> 
      <div class="row">

        <!-- sidebar -->
        @include('computer.home.sidebar_right')
        <!-- sidebar -->

        <!-- content -->
        <div class="col-md-9">

          <div id="category-post-section" class="category-post-section">
          @foreach($list_product_cat as $itemproduct)
          <!-- products list -->
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" style="padding-right:3px; padding-left: 3px;">
                 <div class="product-item">
                                  <div class="pi-img-wrapper">
                                    <img src="{{ asset('public/img/product/'.$itemproduct->image) }}" alt="Berry Lace Dress" width="300px" height="200px">
                                    <div>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                                      <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                    </div>
                                  </div>
                                  <p class="product-name"><a href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a></p>
                                   <div class="product-price">
                                        <span>{{ format_curency($itemproduct->price) }}</span>
                                        @if($itemproduct->price_compare != 0)
                                            <span class="old_price">{{ format_curency($itemproduct->price_compare) }} </span>
                                        @endif
                                    </div>
                                        @if($itemproduct->quantity != 0)
                                            <button class="btn btn_add_cart_main" idproduct="{{ $itemproduct->idproduct }}" base_url="{{ route('home.showProduct') }}" token="{{ csrf_token() }}"><i class="fa fa-cart-plus fa-1x"><span> MUA NGAY</span></i></button>
                                        @else
                                            <a class="btn btn_add_cart_main" href="#"><i class="fa fa-comments-o fa-1x"><span> LIÊN HỆ</span></i></a>
                                        @endif
                                  <div class="sticker sticker-new" style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">
                                      
                                  </div>
                                </div>
            </div>
            <!-- products list -->
          @endforeach
          </div>

          <!-- pagination -->
          <div class="page_bottom">
            {!! $list_product_cat->render() !!} 
            <!-- <p class="info">{{ trans('category.pagination_show') }} 20 {{ trans('category.pagination_of') }} 300 {{ trans('category.pagination_product') }}</p> -->
          </div>
          <!-- pagination -->


        </div>
        <!-- content -->

      </div> 
    <!-- detail category -->
 
</div>
@endsection() 