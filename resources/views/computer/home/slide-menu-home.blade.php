   <!-- Slider Section -->
    <div class="container hidden-xs hidden-sm hidden-md"> 
        <div id="slider-section" class="section-padding section-white slider-section">
            <div class="col-md-12">
                <div class="row"> 
                    <div class="col-md-12" >
                        <!-- slider1-container -->
                        <div id='coin-slider'>
                        @foreach($slides as $row)
                          <a href="{{$row->linknew}}" target="_blank">
                            <img width="100%" src='{{asset('public/img/slide/'.$row->img)}}'>
                          </a>
                        @endforeach                  
                        </div>
                    </div>
                </div>
                    
                <div class="row">                    
                    @foreach($adverts_bottom as $item)
                        <div class="col-md-4 index-ads">
                            <div class="nn-banner-qc-index">
                              <a href="{{ $item->link }}"><img src="{{ asset('public/img/advert/'.$item->img) }}"></a>
                              <!-- <p>Banner quảng cáo</p> -->
                            </div>
                        </div>
                    @endforeach                    
                </div>
            </div>
        </div>
    </div>
    <!-- slider-container1 /- -->
    <script type="text/javascript">
      $(document).ready(function() {
        $('#coin-slider').coinslider({ width:1300 ,height: 520, navigation: true,showNavigationButtons: true, delay: 2000 });
      });
    </script>
    <!-- Slider Section /- -->