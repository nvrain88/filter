@extends('computer.home.master')
@section('title', (!empty($trademark)?$trademark->name:""))
@section('seo_keyword', (!empty($trademark)?$trademark->name:""))
@section('seo_description', (!empty($trademark)?$trademark->name:""))
@section('seo_url', url()->current())
@section('content')

<div class="wrapper_main container 1111111">
    <!-- quang cáo -->

    <!-- breadcrumb  --> 
      <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li>{{ trans('index.home') }}</li>
          <li><a href=""><b class="fa fa-chevron-right"></b>{{ $trademark->name }}</a></li>
        </ul>
      </div> 
    <!-- breadcrumb  -->



    <!-- detail category --> 
      <div class="row">

        <!-- content -->
        <div class="col-md-9">

          <div id="category-post-section" class="category-post-section">
          @foreach($product as $itemproduct)
          <!-- products list -->
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4" style="padding-right:3px; padding-left: 3px;">
                 <div class="product-item">
                    <div class="pi-img-wrapper">
                      <img src="{{ asset('public/img/product/'.$itemproduct->image) }}" alt="Berry Lace Dress" width="300px" height="200px">
                      <div>
                        <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                        <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                      </div>
                    </div>
                    <p class="product-name"><a href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a></p>
                     <div class="product-price">
                                        <span>{{ format_curency($itemproduct->price) }}</span>
                                        @if($itemproduct->price_compare != 0)
                                            <span class="old_price">{{ format_curency($itemproduct->price_compare) }} </span>
                                        @endif
                                    </div>
                                        @if($itemproduct->quantity != 0)
                                            <button class="btn btn_add_cart_main" idproduct="{{ $itemproduct->idproduct }}" base_url="{{ route('home.showProduct') }}" token="{{ csrf_token() }}"><i class="fa fa-cart-plus fa-1x"><span> MUA NGAY</span></i></button>
                                        @else
                                            <a class="btn btn_add_cart_main" href="#"><i class="fa fa-comments-o fa-1x"><span> LIÊN HỆ</span></i></a>
                                        @endif
                    <div class="sticker sticker-new" style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">
                        
                    </div>
                  </div>
            </div>
            <!-- products list -->
          @endforeach
          </div>

          <!-- pagination -->
          <div class="page_bottom">
            {!! $product->render() !!} 
            <!-- <p class="info">{{ trans('category.pagination_show') }} 20 {{ trans('category.pagination_of') }} 300 {{ trans('category.pagination_product') }}</p> -->
          </div>
          <!-- pagination -->


        </div>
        <!-- content -->

        <!-- sidebar -->
        @include('computer.home.sidebar_right')
        <!-- sidebar -->

      </div> 
    <!-- detail category -->
 
</div>
@endsection() 