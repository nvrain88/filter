@extends('computer.home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('css')
    <style>
        .box-title {
            padding: 10px 0;
            line-height: 32px;
            margin: 0;
            background: transparent;
        }

        .box-xem-them-opt-2 {
            /*position: absolute;*/
            display: -webkit-box;
            top: 10px;
            /*right: 0;*/
            height: 50px;
            padding: 0;
            text-align: center;
            line-height: 32px;
            cursor: pointer;
            /*width: auto;*/
            /*max-width: calc(100% - 295px);*/
            overflow-y: hidden;
            -webkit-overflow-scrolling: touch;
            overflow-scrolling: touch;
            white-space: nowrap;
            overflow-x: overlay;
        }

        .view_pc .search-trend {
            position: relative;
            display: block;
            margin-bottom: 20px;
        }

        .view_pc .box-xem-them-opt-2 a.active {
            background: #d62329;
            color: #fff;
        }

        .view_pc .box-xem-them-opt-2 a {
            width: auto;
            color: #333;
            font-size: 14px;
            height: 32px;
            line-height: 32px;
            background: #f0f0f0;
            padding: 0 10px;
            border-radius: 3px;
            margin-left: 5px;
        }

        .view_pc .box {
            position: relative;
            display: block;
        }

        .view_pc a {
            text-decoration: none;
            transition: all .3s ease 0s;
            cursor: pointer;
        }

        .view_pc .search-trend .box-cate-wrapper {
            margin: 0;
            overflow: hidden;
            margin-top: 10px;
        }
        
        .view_pc .search-trend .box-cate-wrapper, .view_pc .search-trend .box-cate-wrapper .col-md-12{
            padding-left: 0px;
            padding-right: 0px;
        }

        .view_pc .search-trend .box-cate-wrapper .item-cate {
            position: relative;
            display: block;
            float: left;
            /*width: calc(20% - 6px);*/
            /*margin: 0 6px 10px 0;*/
            margin-bottom: 15px;
            overflow: hidden;
            vertical-align: bottom;
            padding-left: 0px;
        }
        
        .view_pc .search-trend .box-cate-wrapper .item-cate:nth-child(4), .view_pc .search-trend .box-cate-wrapper .item-cate:nth-child(8){
            padding-right: 0px;
        }

        .view_pc .search-trend .box-cate-wrapper .item-cate .box-inner {
            background: #f0f0f0;
            overflow: hidden;
            padding: 15px 5px 15px 10px;
        }

        .view_pc .search-trend .box-cate-wrapper .item-cate .box-inner .box-img-l {
            position: relative;
            display: block;
            float: left;
            width: 100px;
            height: 100px;
            border-radius: 8px;
            overflow: hidden;
        }

        .view_pc .search-trend .box-cate-wrapper .item-cate .box-inner .box-text-r {
            position: relative;
            display: block;
            float: left;
            width: calc(100% - 100px);
            padding-left: 6px;
            text-align: left;
        }

        .view_pc .search-trend .box-cate-wrapper .item-cate .box-inner .box-text-r .title-cate {
            position: relative;
            display: block;
            font-size: 14px;
            line-height: 21px;
            color: #242424;
            font-weight: normal;
        }
        
        .view-pc .title-section{
            line-height: 1.5;
        }
    </style>
@endsection
@section('content')

    <div class="wrapper_main container view_pc">
        <!-- quang cáo -->

        <!-- breadcrumb  -->
        <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
          <li><i class="fa fa-chevron-right"></i><span>Xu hướng tìm kiếm</span></li>
        </ul>
      </div> 
        <!-- breadcrumb  -->


        <!-- detail category -->
        <div class="row">
            <!-- content -->
            <div class="col-md-12 search-trend">
                <div class="box">
                    <div class="row">
                        <div class="col-md-12">
                        <div class="box-title col-md-2">
                            <h4 class="title-section">Xu hướng tìm kiếm</h4>
                        </div>
                        <div class="box-xem-them-opt-2 col-md-10">
                            <a href="{{route('home.trendingAll')}}" class="box @if($slug === '') active @endif"
                               title="Tất cả">Tất cả</a>
                            @foreach($modproducts as $category)
                                <a href="{{route('home.trending',['slug' => $category->slug])}}"
                                   class="box @if($slug === $category->slug) active @endif"
                                   title="">{{$category->modname}}</a>
                            @endforeach
                        </div>
                        <div class="clearfix"></div>
                        </div>
                    </div>
                    <div class="row box-cate-wrapper">
                        <div class="col-md-12">
                        @for($i = 1; $i < 6; $i++)
                            <div class="box item-cate col-md-3 col-xs-12">
                                <div class="box box-inner">
                                    <a href="https://store.ngoisao.net/quan-nu" class="box-img-l" title="Quần nữ ">
                                        <img src="https://i-shop.vnecdn.net/crop/174/174/images/2018/10/02/5bb3172e33588-quannu.jpg"
                                             alt="Quần nữ " class="responsive">
                                    </a>
                                    <div class="box-text-r">
                                        <a href="#" title="Quần nữ " class="box title-cate">Quần nữ </a>
                                    </div>
                                </div>
                            </div>
                        @endfor
                        </div>
                    </div>
                    <div class="box-title">
                        <h4 class="title-section">Sản phẩm bán chạy</h4>
                    </div>
                    <div class="box-product">

                        @foreach($products as $itemproduct)
                            <div class="col-xs-6 col-sm-3 col-md-3 col-lg-3"
                                 style="padding-right:3px; padding-left: 3px;">
                                <div class="product-item">
                                    <div class="pi-img-wrapper">
                                        <img src="{{ asset('public/img/product/'.$itemproduct->image) }}"
                                             alt="Berry Lace Dress" width="300px" height="200px">
                                        <div>
                                            <a href="{{ url('san-pham/'.$itemproduct->slug) }}" class="btn">Xem</a>
                                            <a href="{{ url('san-pham/'.$itemproduct->slug) }}"
                                               class="btn share_link_fb"><span class="fa fa-share-alt"></span></a>
                                        </div>
                                    </div>
                                    <p class="product-name"><a
                                                href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a>
                                    </p>
                                    <div class="product-price">
                                        <span>{{ format_curency($itemproduct->price) }}</span>
                                        @if($itemproduct->price_compare != 0)
                                            <span class="old_price">{{ format_curency($itemproduct->price_compare) }} </span>
                                        @endif
                                    </div>
                                    @if($itemproduct->quantity != 0)
                                        <button class="btn btn_add_cart_main" idproduct="{{ $itemproduct->idproduct }}"
                                                base_url="{{ route('home.showProduct') }}" token="{{ csrf_token() }}"><i
                                                    class="fa fa-cart-plus fa-1x"><span> MUA NGAY</span></i></button>
                                    @else
                                        <a class="btn btn_add_cart_main" href="#"><i
                                                    class="fa fa-comments-o fa-1x"><span> LIÊN HỆ</span></i></a>
                                    @endif
                                    <div class="sticker sticker-new"
                                         style="background: url( {{ url('public/img/listproduct/'.$itemproduct->tinhtrang['sttimg'])    }} ) no-repeat;">

                                    </div>
                                </div>
                            </div>
                        @endforeach

                    </div>
                </div>

            </div>
            <!-- content -->

            <!-- sidebar -->
            <!-- sidebar -->

        </div>
        <!-- detail category -->

    </div>
@endsection()
@section('css')

@stop