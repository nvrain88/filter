@extends('computer.home.master')
@section('title', (!empty($contact)?$contact->seo_title:""))
@section('seo_keyword', (!empty($contact)?$contact->seo_keyword:""))
@section('seo_description', (!empty($contact)?$contact->seo_description:""))
@section('seo_image', (!empty($contact)?asset($contact->seo_image):""))
@section('seo_url', url()->current())
@section('css')
    <style>
        #hasaki_go_page {
            width: 800px;
            margin: 0 auto;
        }
        .title_hasakigo {
            font-size: 24px;
            font-weight: 700;
            margin-bottom: 10px;
        }
        .lead_hasakigo {
            font-weight: 700;
            margin-bottom: 10px;
        }
        .space_bottom_15 {
            margin-bottom: 15px;
        }
        .tb_content_hasakigo {
            width: 600px;
            margin-left: auto;
            margin-right: auto;
            border: 1px solid #cccccc;
        }

        .space_bottom_20 {
            margin-bottom: 20px;
        }
        table {
            background-color: transparent;
        }
        table {
            border-collapse: collapse;
            border-spacing: 0;
        }
        .tb_content_hasakigo tr:nth-child(2n+1) td {
            background: #f7f7f7;
        }
        .tb_content_hasakigo td {
            width: 50%;
            padding: 5px 10px;
            text-align: center;
            border-bottom: 1px solid #eee;
            border-right: 1px solid #eee;
            vertical-align: middle;
        }
    </style>
@endsection
@section('content')

    <div class="wrapper_main container view_pc">
        <!-- quang cáo -->

        <!-- breadcrumb  -->
        <div class="breadcrumb nn-header-breadcrumb">
        <ul>
          <li><a href="https://shop.lavendercare.vn/">{{ trans('index.home') }}</a></li>
          <li><i class="fa fa-chevron-right"></i><span>Giao Hàng 2H</span></li>
        </ul>
      </div> 
        <!-- breadcrumb  -->


        <!-- detail category -->
        <div class="row">
            <!-- content -->
            <div class="col-md-12 search-trend">
                <div id="hasaki_go_page">
                    <div class="title_hasakigo">Chính Sách Và Cước Phí Dịch Vụ HasakiGo - Giao Hàng 2H</div>
                    <div class="lead_hasakigo txt_16">Thông thường sau khi đặt hàng online, bạn phải hồi hộp chờ đợi vài ngày mới cầm được món hàng trên tay. Tin vui dành cho bạn, Hasaki.vn vừa ra mắt dịch vụ giao hàng <strong>nội thành HCM</strong> <strong>2H</strong>. Hãy trải nghiệm ngay cảm giác mua sắm <strong>2H</strong> đã có hàng tận tay, chỉ với <strong>9.000đ</strong> cho mỗi đơn hàng.</div>
                    <div class="content_hasakigo">
                        <div class="space_bottom_15"><strong>I- Thể lệ chương trình:</strong> <br>
                            <!--                    - Áp dụng cho tất cả các ngày trong tuần, kể cả Chủ nhật  <br>-->
                            - Áp dụng 6 ngày trong tuần, từ thứ 2 đến thứ 7  <br>
                            - Thời gian dự kiến nhận hàng được chia theo <strong>11</strong> khung giờ như sau: <br>
                        </div>
                        <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                            <tbody><tr>
                                <td><strong>Thời gian đặt hàng</strong></td>
                                <td><strong>Dự kiến nhận hàng</strong></td>
                            </tr>
                            <tr>
                                <td>Từ 00:01 - 08:00</td>
                                <td>Trước 10:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 08:01 - 09:00</td>
                                <td>Trước 11:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 09:01 - 10:00</td>
                                <td>Trước 12:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 10:01 - 11:00</td>
                                <td>Trước 13:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 11:01 - 12:00</td>
                                <td>Trước 14:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 12:01 - 13:00</td>
                                <td>Trước 15:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 13:01 - 14:00</td>
                                <td>Trước 16:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 14:01 - 15:00</td>
                                <td>Trước 17:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 15:01 - 16:00</td>
                                <td>Trước 18:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 16:01 - 18:00</td>
                                <td>Trước 20:00 cùng ngày</td>
                            </tr>
                            <tr>
                                <td>Từ 18:01 - 00:00</td>
                                <td>Trước 10:00 ngày kế tiếp</td>
                            </tr>
                            </tbody></table>
                        <div class="space_bottom_15">
                            <strong>II- Điều kiện áp dụng dịch vụ Giao Hàng 2H</strong> <br>
                            <strong>1. Khách hàng: </strong> <br>
                            Dịch vụ được áp dụng cho mọi khách hàng của Hasaki<br>
                            <strong>2. Khu vực: </strong> <br>
                            Dịch vụ được áp dụng tại hầu hết các khu vực thuộc địa bàn TP. Hồ Chí Minh. Cụ thể như sau:<br>
                        </div>
                        <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                            <tbody>


                            <tr id="row-district-id-1">
                                <td>Quận 1</td>
                                <td id="cell-ward-id-">
                                    Toàn bộ khu vực

                                </td>
                            </tr>





                            <tr id="row-district-id-3">
                                <td>Quận 3</td>
                                <td id="cell-ward-id-">
                                    Toàn bộ khu vực

                                </td>
                            </tr>



                            <tr id="row-district-id-4">
                                <td>Quận 4</td>
                                <td id="cell-ward-id-">
                                    <div class="item_line">Phường 1</div>
                                    <div class="item_line">Phường 15</div>
                                    <div class="item_line">Phường 2</div>
                                    <div class="item_line">Phường 3</div>
                                    <div class="item_line">Phường 4</div>
                                    <div class="item_line">Phường 14</div>
                                    <div class="item_line">Phường 5</div>
                                    <div class="item_line">Phường 10</div>
                                    <div class="item_line">Phường 8</div>
                                    <div class="item_line">Phường 6</div>
                                    <div class="item_line">Phường 9</div>

                                </td>
                            </tr>



                            <tr id="row-district-id-5">
                                <td>Quận 5</td>
                                <td id="cell-ward-id-27256">
                                    Toàn bộ khu vực

                                </td>
                            </tr>



                            <tr id="row-district-id-6">
                                <td>Quận 6</td>
                                <td id="cell-ward-id-27256">
                                    <div class="item_line">Phường 3</div>
                                    <div class="item_line">Phường 8</div>
                                    <div class="item_line">Phường 4</div>
                                    <div class="item_line">Phường 1</div>
                                    <div class="item_line">Phường 2</div>
                                    <div class="item_line">Phường 5</div>
                                    <div class="item_line">Phường 12</div>
                                    <div class="item_line">Phường 6</div>
                                    <div class="item_line">Phường 9</div>
                                    <div class="item_line">Phường 14</div>

                                </td>
                            </tr>



                            <tr id="row-district-id-7">
                                <td>Quận 7</td>
                                <td id="cell-ward-id-27346">
                                    <div class="item_line">Phường Tân Phong</div>
                                    <div class="item_line">Phường Tân Phú</div>
                                    <div class="item_line">Phường Bình Thuận</div>
                                    <div class="item_line">Phường Tân Hưng</div>
                                    <div class="item_line">Phường Tân Kiểng</div>
                                    <div class="item_line">Phường Tân Thuận Tây</div>

                                </td>
                            </tr>



                            <tr id="row-district-id-8">
                                <td>Quận 8</td>
                                <td id="cell-ward-id-27466">
                                    <div class="item_line">Phường 14</div>
                                    <div class="item_line">Phường 12</div>
                                    <div class="item_line">Phường 13</div>
                                    <div class="item_line">Phường 10</div>
                                    <div class="item_line">Phường 9</div>
                                    <div class="item_line">Phường 11</div>
                                    <div class="item_line">Phường 3</div>
                                    <div class="item_line">Phường 1</div>
                                    <div class="item_line">Phường 2</div>
                                    <div class="item_line">Phường 8</div>

                                </td>
                            </tr>





                            <tr id="row-district-id-10">
                                <td>Quận 10</td>
                                <td id="cell-ward-id-27388">
                                    Toàn bộ khu vực

                                </td>
                            </tr>



                            <tr id="row-district-id-11">
                                <td>Quận 11</td>
                                <td id="cell-ward-id-27388">
                                    Toàn bộ khu vực

                                </td>
                            </tr>





                            <tr id="row-district-id-13">
                                <td>Quận Tân Bình</td>
                                <td id="cell-ward-id-27388">
                                    Toàn bộ khu vực

                                </td>
                            </tr>



                            <tr id="row-district-id-14">
                                <td>Quận Tân Phú</td>
                                <td id="cell-ward-id-27388">
                                    <div class="item_line">Phường Tân Thới Hòa</div>
                                    <div class="item_line">Phường Hiệp Tân</div>
                                    <div class="item_line">Phường Hòa Thạnh</div>
                                    <div class="item_line">Phường Phú Trung</div>
                                    <div class="item_line">Phường Phú Thạnh</div>
                                    <div class="item_line">Phường Phú Thọ Hòa</div>
                                    <div class="item_line">Phường Tân Thành</div>
                                    <div class="item_line">Phường Tân Quý</div>
                                    <div class="item_line">Phường Tây Thạnh</div>
                                    <div class="item_line">Phường Tân Sơn Nhì</div>

                                </td>
                            </tr>



                            <tr id="row-district-id-15">
                                <td>Quận Phú Nhuận</td>
                                <td id="cell-ward-id-27010">
                                    <div class="item_line">Phường 13</div>
                                    <div class="item_line">Phường 12</div>
                                    <div class="item_line">Phường 14</div>
                                    <div class="item_line">Phường 17</div>
                                    <div class="item_line">Phường 11</div>
                                    <div class="item_line">Phường 10</div>
                                    <div class="item_line">Phường 8</div>
                                    <div class="item_line">Phường 2</div>
                                    <div class="item_line">Phường 1</div>
                                    <div class="item_line">Phường 3</div>
                                    <div class="item_line">Phường 7</div>
                                    <div class="item_line">Phường 9</div>
                                    <div class="item_line">Phường 5</div>
                                    <div class="item_line">Phường 4</div>

                                </td>
                            </tr>



                            <tr id="row-district-id-16">
                                <td>Quận Gò Vấp</td>
                                <td id="cell-ward-id-27043">
                                    Toàn bộ khu vực

                                </td>
                            </tr>



                            <tr id="row-district-id-17">
                                <td>Quận Bình Thạnh</td>
                                <td id="cell-ward-id-27043">
                                    <div class="item_line">Phường 24</div>
                                    <div class="item_line">Phường 11</div>
                                    <div class="item_line">Phường 26</div>
                                    <div class="item_line">Phường 12</div>
                                    <div class="item_line">Phường 5</div>
                                    <div class="item_line">Phường 7</div>
                                    <div class="item_line">Phường 6</div>
                                    <div class="item_line">Phường 14</div>
                                    <div class="item_line">Phường 2</div>
                                    <div class="item_line">Phường 1</div>
                                    <div class="item_line">Phường 3</div>

                                </td>
                            </tr>
















                            </tbody>
                        </table>
                        <div class="space_bottom_15"><strong>Hasaki</strong> sẽ cập nhật khu vực giao hàng 2H theo yêu cầu khách hàng phù hợp với điều kiện vận hành của công ty.
                        </div>
                        <div class="space_bottom_15"><strong>3. Cước phí:</strong><br>
                            <table cellpadding="0" cellspacing="0" border="0" class="tb_content_hasakigo space_bottom_20">
                                <tbody>
                                <tr>
                                    <td><strong>Điều kiện đơn hàng</strong></td>
                                    <td><strong>Mức phí</strong></td>
                                </tr>
                                <!-- <tr>
                                    <td>Đơn hàng có tổng giá trị sản phẩm Bé - Đồ Chơi từ 90.000đ</td>
                                    <td>0đ (trợ giá 19.000đ)</td>
                                </tr>
                                <tr>
                                    <td>Đơn hàng có tổng giá trị sản phẩm Bé - Đồ Chơi nhỏ hơn 90.000đ</td>
                                    <td>10.000đ (trợ giá 19.000đ)</td>
                                </tr> -->
                                <!-- <tr>
                                    <td>Đơn hàng khác từ 90.000đ</td>
                                    <td>19.000đ</td>
                                </tr>
                                <tr>
                                    <td>Đơn hàng khác nhỏ hơn 90.000đ</td>
                                    <td>29.000đ</td>
                                </tr> -->
                                <tr>
                                    <td>Đơn hàng từ 90.000đ</td>
                                    <td>9.000đ</td>
                                </tr>
                                <tr>
                                    <td>Đơn hàng nhỏ hơn 90.000đ</td>
                                    <td>19.000đ</td>
                                </tr>

                                </tbody></table>
                        </div>

                        <div class="space_bottom_15">
                            <strong>Hasaki.vn</strong> ra mắt dịch vụ giao hàng 2H với mong muốn đáp ứng nhiều hơn nữa nguyện vọng của khách hàng, tuy nhiên thực tế vận hành có thể sẽ phát sinh những rủi ro ngoài ý muốn.Trong trường hợp này, mọi ý kiến thắc mắc hoặc góp ý vui lòng liên hệ bộ phận Chăm Sóc Khách Hàng qua Hotline: <strong>1900 636900</strong> (8h - 21h) hoặc Email: <strong>hotro@hasaki.vn. </strong>
                        </div>

                    </div>
                </div>
            </div>
            <!-- content -->

            <!-- sidebar -->
            <!-- sidebar -->

        </div>
        <!-- detail category -->

    </div>
@endsection()
@section('css')

@stop