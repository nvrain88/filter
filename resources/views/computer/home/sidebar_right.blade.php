<?php
session_start();
?>

<div class="col-md-3 AHIHI">
    <?php use App\Http\Controllers\HomeController;

    ?>


    {{--  Filter--}}

    <div class="block_sidebar block_product_sidebar">
        <h2 class="bg_hbh title">LỌC SẢN PHẨM</h2>
        <div class="content">
            <div class="filter filter_price">
                <div class="left-menu">

                    <?php

	                $fullLink = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ?
			                "https" : "http") . "://" . $_SERVER['HTTP_HOST'] .
		                $_SERVER['REQUEST_URI'];
	                ?>

                    <form class="filter_form_td" method="get" action="/filterAll" accept-charset="UTF-8">

                        <div class="panel-group" id="trademarks">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse10">Theo giá sản phẩm</a>
                                    </h4>
                                </div>
                                <div id="collapse10" class="panel-collapse collapse in">
                                    <input <?php if ($productPrice == '0,249999') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="0,249999" name="productPrice"> Dưới 250k<br/>

                                    <input <?php if ($productPrice == '250000,499999') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="250000,499999" name="productPrice"> 250k - 500k<br/>

                                    <input <?php if ($productPrice == '500000,1000000') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="500000,1000000" name="productPrice"> 500k - 1tr<br/>

                                    <input <?php if ($productPrice == '1000001,2000000') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="1000001,2000000" name="productPrice"> 1tr - 2tr<br/>

                                    <input <?php if ($productPrice == '2000001,3000000') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="2000001,3000000" name="productPrice"> 2tr - 3tr<br/>

                                    <input <?php if ($productPrice == '') echo 'checked'; else{ echo '';} ?>
                                            type="radio" value="" name="productPrice"> Tất cả<br/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="trademarks">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse9">Theo tình trạng</a>
                                    </h4>
                                </div>
                                <div id="collapse9" class="panel-collapse collapse in">
                                    @foreach( $productstt as $key => $row)
                                        <input <?php if ($productStatus == $row->id ) echo 'checked'; ?>
                                                type="radio" value="{{$row->id}}" name="productStatus"> {{$row->sttname}}<br/>
                                    @endforeach
                                    <input <?php if ($productStatus =='') echo 'checked'; ?> type="radio" value="" name="productStatus"> Tất cả<br/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="trademarks">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse8">Theo thời gian</a>
                                    </h4>
                                </div>
                                <div id="collapse8" class="panel-collapse collapse in">
                                    <input <?php if ($orderTime === 'asc') echo 'checked'; ?> type="radio" value="asc" name="orderTime"> Mới đến cũ<br/>
                                    <input type="radio" value="desc" <?php if ($orderTime === 'desc') echo 'checked'; ?> name="orderTime"> Cũ đến mới<br/>
                                    <input type="radio" value="" <?php if ($orderTime === '') echo 'checked'; ?> name="orderTime"> Tất cả<br/>
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="trademarks">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse1">Theo Thương hiệu</a>
                                    </h4>
                                </div>
                                <div id="collapse1" class="panel-collapse collapse <?= !empty($trademarksSlt) ? 'in' : 'in' ?>">
                                    @foreach( $trademarks as $key => $row)
                                        <input class="trademarks trademarks-{{$key}}" type="checkbox"
                                                      <?= (!empty($trademarksSlt) && in_array( $row->id ,$trademarksSlt))? 'checked' : '' ?>
                                                      name="trademarks[{{$row->id}}]"
                                                      value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="panel-group" id="producttypes">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse2">Theo loại sản phẩm</a>
                                    </h4>
                                </div>
                                <div id="collapse2" class="panel-collapse collapse <?= !empty($producttypesSlt) ? 'in' : 'in' ?>">
                                    @foreach( $producttypes as $key => $row)
                                    <input class="producttypes producttypes-{{$key}}"
                                                  type="checkbox" name="producttypes[{{$row->id}}]"
                                            <?= (!empty($producttypesSlt) && in_array( $row->id ,$producttypesSlt))? 'checked' : '' ?>
                                                  value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="panel-group" id="skintypes">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse4">Theo loại da</a>
                                    </h4>
                                </div>
                                <div id="collapse4" class="panel-collapse collapse <?= !empty($skintypesSlt) ? 'in' : 'in' ?>">
                                    @foreach( $skintypes as $key => $row)
                                    <input class="skintypes skintypes-{{$key}}"
                                                  type="checkbox" name="skintypes[{{$row->id}}]"
                                            <?= (!empty($skintypesSlt) && in_array( $row->id ,$skintypesSlt))? 'checked' : '' ?>
                                                  value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="panel-group" id="origins">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse5">Theo xuất xứ</a>
                                    </h4>
                                </div>
                                <div id="collapse5" class="panel-collapse collapse <?= !empty($originsSlt) ? 'in' : 'in' ?>">
                                    @foreach( $origins as $key => $row)
                                    <input class="origins origins-{{$key}}" type="checkbox"
                                                  name="origins[{{$row->id}}]"
                                            <?= (!empty($originsSlt) && in_array( $row->id ,$originsSlt))? 'checked' : '' ?>
                                                  value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="panel-group" id="characteristics">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse6">Theo đặc tính</a>
                                    </h4>
                                </div>
                                <div id="collapse6" class="panel-collapse collapse <?= !empty($characteristicsSlt) ? 'in' : 'in' ?>">
                                    @foreach( $characteristics as $key => $row)
                                    <input class="characteristics characteristics-{{$key}}" type="checkbox"
                                                  name="characteristics[{{$row->id}}]"
                                            <?= (!empty($characteristicsSlt) && in_array( $row->id ,$characteristicsSlt))? 'checked' : '' ?>
                                                  value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                        <div class="panel-group" id="skinproblems">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse7">Theo vấn đề về da</a>
                                    </h4>
                                </div>
                                <div id="collapse7" class="panel-collapse collapse <?= !empty($skinproblemsSlt) ? 'in' : 'in' ?>">
                                    @foreach( $skinproblems as $key => $row)
                                    <input class="skinproblems skinproblems-{{$key}}" type="checkbox"
                                                  name="skinproblems[{{$row->id}}]"
                                            <?= (!empty($skinproblemsSlt) && in_array( $row->id ,$skinproblemsSlt))? 'checked' : '' ?>
                                                  value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!--    ket cau -->
                        <div class="panel-group" id="structures">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h4 class="panel-title">
                                        <a data-toggle="collapse" href="#collapse3">Theo kết cấu</a>
                                    </h4>
                                </div>
                                <div id="collapse3" class="panel-collapse collapse <?= !empty($structuresSlt) ? 'in' : 'in' ?>">
                                    @foreach( $structures as $key => $row)
                                    <input class="structures structures-{{$key}}"
                                           type="checkbox" name="structures[{{$row->id}}]"
                                        <?= (!empty($structuresSlt) && in_array( $row->id ,$structuresSlt))? 'checked' : '' ?>
                                           value="{{$row->id}}"> {{ $row->title }}<br/>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <!--hidden slug-->
                        <input type="hidden" name="slug" value="<?= empty($path_var) ? '' : $path_var ?>">


{{--                        <fieldset id="loctatca">--}}
{{--                            <label>Reset bộ lọc</label><br/>--}}
{{--                            <a href=""/>Reset</fieldset>--}}
{{--                        </fieldset>--}}

                        <br/>


                    <div class="submit_form">

                        <input type="submit" value="Lọc Sản Phẩm" id="filerAll" />

                        <input class="reset" type="button" value="Bỏ lọc">
                    </div>

                    </form>

                </div>
            </div>
        </div>
    </div>

        <script>
            // $('.filter_form_td .submit_form input').click(function () {
            //     var price = $(this).closest('form').find('#range input:checked').val() ? $(this).closest('form').find('#range input:checked').val() : '1,9999999999999';
            //     var status = $(this).closest('form').find('#status input:checked').val();
            //     if (status != '5'){
            //         status = $(this).closest('form').find('#status input:checked').val();
            //     }else{
            //         status = '';
            //     }
            //     var times = $(this).closest('form').find('#timess input:checked').val();
            //     // console.log(price);
            //     // console.log('status' + status);
            //     // console.log('times' + times);
            //
            //     if(price){
            //         window.location.href = "http://localhost:8099/filter?range="+price;
            //     }
            //
            //     if(price && status){
            //         if (status != '5'){
            //             console.log('111');
            //             window.location.href = "http://localhost:8099/filter?range="+price+'&stt='+status;
            //         } else{
            //             console.log('2222');
            //             window.location.href = "http://localhost:8099/filter?range="+price;
            //         }
            //
            //     }
            //     if(price && times){
            //         window.location.href = "http://localhost:8099/filter?range="+price+'&time='+times;
            //     }
            //     if(price && status && times){
            //         window.location.href = "http://localhost:8099/filter?range="+price+'&stt='+status+'&time='+times;
            //     }
            //
            // });


            $('input.reset').click(function () {
                $(".skinproblems").each(function () {
                    $(this).prop("checked", false);
                });
                $(".structures").each(function () {
                    $(this).prop("checked", false);
                });
                $(".characteristics").each(function () {
                    $(this).prop("checked", false);
                });
                $(".origins").each(function () {
                    $(this).prop("checked", false);
                });
                $(".skintypes").each(function () {
                    $(this).prop("checked", false);
                });
                $(".producttypes").each(function () {
                    $(this).prop("checked", false);
                });
                $(".trademarks").each(function () {
                    $(this).prop("checked", false);
                });
                $('input[name="orderTime"]').val('');
                $('input[name="productStatus"]').val('');
                $('input[name="productPrice"]').val('');
                $("#filerAll").click();
            });
        </script>

    <!-- block_sidebar menu -->
    <div class="block_sidebar block_product_sidebar">
        <h2 class="bg_hbh title">{{ trans('index.category') }}</h2>
        <div class="content">

            <!-- col menu category 1 -->
            <div class="item_content">
                {{--        <div class="header_content">  --}}
                {{--          <button type="button" class="btn_collapsed" data-toggle="collapse" data-target="#item_content_menu_1"><i class="fa fa-minus"></i></button>--}}
                {{--          <h1 class="navbar-brand title_item">{{ trans('index.category') }} {{ trans('index.product') }}</h1>--}}
                {{--        </div>--}}

                {{--        <div id="item_content_menu_1" class="collapse2">--}}
                {{--          <ul class="list_menu_category">--}}
                {{--            @foreach($listproducts as $item)--}}
                {{--              <li><a href="{{ url('loai-san-pham').'/'.$item->slug }}"><i class="fa fa-angle-right"></i> {{ $item->trname }}</a></li>--}}
                {{--            @endforeach--}}
                {{--          </ul>--}}
                {{--        </div>--}}

                <div id="item_content_menu_1" class="collapse2">
                    <ul class="list_menu_category">

                        @php $dem = 1; @endphp

                        @foreach($modproducts as $item)

                            <?php
		                        $data1 = HomeController::getcategorybyParent($item->id);
                            ?>
                            <li>

                                <a class="menu_a menu_lv1" href="{{ url('loai-san-pham').'/'.$item->slug }}">{{ $item->trname }}</a>
                            @if (sizeof($data1) > 0)
                                <span class="arrow_span" data-toggle="collapse" data-target="#collapseExample{{$dem}}"
                                      aria-expanded="false" aria-controls="collapseExample">
                                <i class="fa fa-angle-down" aria-hidden="true"></i>
                            @endif
                  </span>

                                <ul class="collapse" id="collapseExample{{$dem}}">
                                    @foreach($data1 as $data)

<!--                                        --><?php //var_dump($data); ?>
                                        <li><a href="{{ url('loai-san-pham').'/'.$data->slug }}"> {{ $data->listname }}</a></li>

                                    @endforeach
                                </ul>
                            </li>
                            @php $dem++; @endphp
                        @endforeach
                    </ul>
                </div>


            </div>
            <!-- col menu category 1 -->

        </div>
    </div>
    <!-- block_sidebar menu -->

    <!-- block_sidebar loc san pham -->

    <!-- block_sidebar  loc san pham -->
    <!-- block_sidebar loc san pham -->
    <div class="block_sidebar block_product_sidebar">
        <h2 class="bg_hbh title">Sản phẩm nổi bật </h2>
        <div class="content">

            <div class="filter_list_product">
                <div class="row">
                    @foreach($list_special_mon_trong_ngay as $itemproduct)
                        <div class="col-xs-12">
                            <div class="item_filter_list_product col-xs-4">
                                <a href="#">
                                    <img class="img-responsive"
                                         src="{{ asset('public/img/product/'.$itemproduct->image) }}" alt="img">
                                </a>
                            </div>
                            <div class="item_filter_list_product col-xs-8 support">
                                <h3 class="pro_title"><a
                                            href="{{ url('san-pham/'.$itemproduct->slug) }}">{{ $itemproduct->name }}</a>
                                </h3>
                                <h3 class="pro_price"><a href="#">
                                        @if($itemproduct->price != 0)
                                            <span>{{ format_curency($itemproduct->price) }}</span>
                                        @else
                                            <span>{{ trans("index.contact") }}</span>
                                        @endif
                                    </a></h3>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

    <!-- block_sidebar loc san pham -->
    <div class="block_sidebar block_product_sidebar">
        <h2 class="bg_hbh title">HỖ TRỢ TRỰC TUYẾN </h2>
        <div class="content">
			<?php $us = DB::table('customers')->where('idgroup', 2)->take(4)->get(); ?>
            <div class="filter_list_product">
                <div class="row">
                    @foreach( $us as $row)
                        <div class="col-xs-12">
                            <div class="item_filter_list_product col-xs-4">
                                <a href="#">
                                    <img class="img-responsive" src="{{url('/public/img/customers/'.$row->cusimg)}}"
                                         alt="img">
                                </a>
                            </div>
                            <div class="item_filter_list_product col-xs-8 support" style="padding-left:1px;">
                                <h3><a href="#" style="font-weight: 600;">{{$row->cusfullname}}</a></h3>
                                <h3><a href="#">Hotline: <span style="color: #EE0000;"> {{$row->cusphone}}</span></a>
                                </h3>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!-- block_sidebar  loc san pham -->



</div>
