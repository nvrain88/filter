<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Trademark;
use App\Slide;
use App\Language;
use File,DateTime;
class TrademarkController extends Controller
{
   /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function ListTrademark(){
        $Slides = Trademark::all();
        $Language = Language::all();
        return view('computer.admin.trademark',['slides'=>$Slides,'langs'=>$Language]);
    }
    public function AddTrademark(Request $request){
        session(['actionuser' => 'add']);        
        $this->validate($request,[
                'nntitle' => 'required',
                'nnavatarfile' => 'image|max:500000',                
            ],[
                'nntitle.required' => 'Bạn cần thêm tên thương hiệu',
                'nnavatarfile.image' => 'Avatar phải là hình ảnh',
                'nnavatarfile.max' => 'Avatar dung lượng quá lớn',

            ]);
        $slide = new Trademark;
        $slide->title = $request->nntitle;
        $slide->status = $request->nnhide;
        if($request->hasFile('nnavatarfile')){
                $file = $request->file('nnavatarfile');
                $nameimg = changeTitle($file->getClientOriginalName()); 
                $hinh = "nt7solution-".str_random(6)."_".$nameimg;
                while(file_exists("public/img/trademark/".$hinh))
                {
                    $hinh = "nt7solution-".str_random(6)."_".$nameimg;
                }
                $file->move("public/img/trademark",$hinh);
                $slide->image = $hinh;
            }else{
                $slide->image = "no-img.png";
            }
        $slide->save();
        return redirect('admin/trademark/list')->with('thongbao','thêm thành công');
    }
    public function EditTrademark(Request $request){
        session(['actionuser' => 'edit','editid'=>$request->ennidslide]);   
        $this->validate($request,[
                'enntitle' => 'required',
                'ennavatarfile' => 'image|max:500000',                
            ],[
                'enntitle.required' => 'Bạn cần thêm tên thương hiệu',
                'ennavatarfile.image' => 'Avatar phải là hình ảnh',
                'ennavatarfile.max' => 'Avatar dung lượng quá lớn',

            ]);
        $slide = Trademark::find($request->ennidslide);
        $slide->title = $request->enntitle;
        $slide->status = $request->ennhide;
        if($request->hasFile('ennavatarfile')){
            $file = $request->file('ennavatarfile');
            $nameimg = changeTitle($file->getClientOriginalName()); 
            $hinh = "nt7solution-".str_random(6)."_".$nameimg;
            while(file_exists("public/img/trademark/".$hinh))
            {
                $hinh = "nt7solution-".str_random(6)."_".$nameimg;
            }
            $file->move("public/img/trademark",$hinh);
            // removefile
            $imgold = $request->ennimguserold;
            if($imgold !="no-img.png"){
                while(file_exists("public/img/trademark/".$imgold))
                {
                    unlink("public/img/trademark/".$imgold);
                }
            }
            
            $slide->image = $hinh;
        }
        $slide->save();
        return redirect('admin/trademark/list')->with('thongbao','sửa thành công');
    }
    public function DleteTrademark(Request $request){
        $product = Product::where('trademark_id',$request->dennidslide)->count();
        if ($product > 0) {
            return redirect('admin/trademark/list')->with('thongbao','Xóa thất bại');
        }
        $slide = Trademark::find($request->dennidslide);
        $slide->delete();
        $imgold = $request->dennimgslide;
            if($imgold !="no-img.png"){
                while(file_exists("public/img/trademark/".$imgold))
                {
                    unlink("public/img/trademark/".$imgold);
                }
            }
        return redirect('admin/trademark/list')->with('thongbao','Xóa thành công');

    }
}
